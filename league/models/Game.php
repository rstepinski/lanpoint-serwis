<?php

class Game extends ActiveRecord
{
	const ERR_SUMMONER_MISMATCH = 0b001;
	const ERR_TEAM_MISMATCH = 0b010;
	
	function getWinner()
	{
		return Team::findOne(['id' => $this->winnerId]);
	}
}