<?php

class Team extends ActiveRecord
{
	const SCENARIO_SEARCH = 'search';
	const SCENARIO_CREATE = 'create';
	const SCENARIO_EDIT = 'edit';
	
	const CLEAR = ['isValid', 'ownerId'];
	
	function attributeLabels()
	{
		return [
			'name' => 'Nazwa',
			'tag' => 'Tag',
		];
	}
	
	function rules()
	{
		switch ($this->scenario)
		{
			case self::SCENARIO_CREATE: return [
				'name' => [
					['required'], 
					['string', 'length' => ['min' => 3, 'max' => 20]], 
					['unique', 'field' => 'name', 'msg' => 'Ta nazwa jest zajęta']
				],
				'tag' => [
					['required'], 
					['string', 'length' => ['min' => 2, 'max' => 4]], 
					['unique', 'field' => 'tag', 'msg' => 'Ten tag jest zajęty']
				],
			];
			case self::SCENARIO_SEARCH: return [];	
			case self::SCENARIO_EDIT: return [
				'name' => [
					['required'], 
					['string', 'length' => ['min' => 3, 'max' => 20]], 
					['unique', 'field' => 'name', 'msg' => 'Ta nazwa jest zajęta']
				],
				'tag' => [
					['required'], 
					['string', 'length' => ['min' => 2, 'max' => 4]], 
					['unique', 'field' => 'tag', 'msg' => 'Ten tag jest zajęty']
				],
			];
		}
	}
	
	function getOwner()
	{
		return User::findOne(['id' => $this->ownerId]);
	}
	
	function getMembers()
	{
		$models = User::find()->where(['team_id' => $this->id, 'is_member' => true])->asArray()->run();
		
		for ($i = 0; $i < count($models); $i++)
		{
			if ($models[$i]->id == $this->ownerId)
			{
				$temp = $models[0];
				$models[0] = $models[$i];
				$models[$i] = $temp;
				break;
			}
		}
		return $models;
	}
	
	function getRequests()
	{
		return User::find()->where(['team_id' => $this->id, 'is_member' => 0])->asArray()->run();
	}
	
	function getIsFull()
	{
		return count($this->members) == 5;
	}
	
	function getIsValid()
	{
		$count = count($this->members) == 5;
		
		$p = [0, 0, 0, 0, 0];
		
		foreach ($this->members as $member)
		{
			if ($member->position < 5)
			{
				$p[$member->position] = 1;
			}
		}
		
		return $count && (($p[0] + $p[1] + $p[2] + $p[3] + $p[4]) == 5);
	}
}