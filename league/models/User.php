<?php

class User extends ActiveRecord implements IdentityInterface
{	
	const ROLE_USER = 0;
	const ROLE_ADMIN = 1;
	
	const AVATAR_SIZE = 400;
	const AVATAR_PATH = 'C:/xampp/htdocs/gorion/league/site/img/avatars/';
	
	const SCENARIO_SIGNUP = 'signup';
	const SCENARIO_LOGIN = 'login';
	const SCENARIO_PAIR = 'pair';
	const SCENARIO_EDIT = 'edit';
	
	const POSITION_TOP = 0;
	const POSITION_MID = 2;
	const POSITION_JUNGLE = 1;
	const POSITION_SUPPORT = 3;
	const POSITION_ADC = 4;
	const POSITION_TBD = 5;
	
	const CLEAR = ['role', 'isVerified', 'isMember', 'avatar', 'teamId', 'tier', 'division', 'summonerId'];
	
	function attributeLabels()
	{
		return [
			'username' => 'Login',
			'password' => 'Hasło',
			'firstName' => 'Imię',
			'lastName' => 'Nazwisko',
			'summonerName' => 'Nazwa przywoływacza',
			'name' => 'Imię i nazwisko',
			'school' => 'Szkoła',
		];
	}
	
	function rules()
	{
		switch ($this->scenario)
		{
			case self::SCENARIO_SIGNUP: return [
				'username' => [
					'required', 
					['string', 'length' => ['max' => 30, 'min' => 3]], 
					['unique', 'field' => 'username', 'msg' => 'Ta nazwa jest zajęta']
				], 
				'email' => [
					'required', 
					'email', 
					['unique', 'field' => 'email', 'msg' => 'Ten email jest już w użyciu']
				], 
				'password' => [
					'required', 
					['string', 'length' => ['max' => 35, 'min' => 5]]
				],
				'firstName' => [
					'required', 
					['string', 'length' => ['max' => 35, 'min' => 5]]
				],
				'lastName' => [
					'required', 
					['string', 'length' => ['max' => 35, 'min' => 5]]
				],
				'school' => [['required']],
			];
			case self::SCENARIO_EDIT: return [
				'firstName' => ['required', ['string', 'length' => ['max' => 35, 'min' => 5]]],
				'lastName' => ['required', ['string', 'length' => ['max' => 35, 'min' => 5]]],
				'school' => [['required']],
			];
			case self::SCENARIO_LOGIN: return ['username' => ['required'], 'password' => ['required']];
			case self::SCENARIO_PAIR: return ['summonerName' => ['required']];
		}
	}
	
	function updateRank()
	{
		if ($this->summonerId)
		{
			$league = App::lolAPI('league-entry', $this->summonerId);
			if ($league == 404)
			{
				$this->tier = 'provisional';
			}
			else
			{
				$this->tier = strtolower($league[0]->tier);
				$this->division = strtolower($league[0]->entries[0]->division);
			}
			$this->update();
		}
	}
	
	function getFullName()
	{
		return $this->firstName.' '.$this->lastName;
	}
	
	function getFullNameAndAlias()
	{
		return $this->firstName.' "'.$this->summonerName.'" '.$this->lastName;
	}
	
	function getTeam()
	{
		if (!$this->isMember) { return null; }
		return Team::findOne(['id' => $this->teamId]);
	}
	
	function getRequestedTeam()
	{
		if ($this->isMember) { return null; }
		return Team::findOne(['id' => $this->teamId]);
	}
	
	function getAvatarPath()
	{
		if ($this->hasAvatar)
		{
			return Url::img('avatars/'.$this->avatar, false);
		}
		else
		{
			if ($this->summonerId)
			{
				return "http://ddragon.leagueoflegends.com/cdn/6.14.2/img/profileicon/$this->profileIcon.png";
			}
			else
			{
				return Url::img('avatars/no_avatar.png', false);
			}
		}
	}
	
	function getHasAvatar()
	{
		return $this->avatar != 'no_avatar.png';
	}
	
	function getRankImage()
	{
		if ($this->tier)
		{
			if ($this->tier == 'provisional' || $this->tier == 'master' || $this->tier == 'challenger')
			{
				return "tier_icons/base_icons/$this->tier.png";
			}
			else
			{
				return "tier_icons/tier_icons/$this->tier"."_$this->division.png";
			}
		}
	}
	
	function getRank()
	{
		return TournamentHelper::calculateRank($this);
	}
	
	function login($password)
	{
		return password_verify($password, $this->password);
	}
}