<?php

return [
	'main' => [
		
		'_DEFINE' => [
			'secondary-darker' => '#0AC9CC',
			'secondary' => '#1CE5E8',
			'secondary-brighter' => '#4EF0F2',
			'bars' => '#159adc',
		],
		'_GFONT-IMPORT' => [
			'Roboto', 'Poiret One', 'Righteous', 'Montserrat', 'Open Sans'
		],
		'color' => [
			'body' => '#f0f0f0', 
			'footer' => '+bars',
			'footer-text' => 'white',
			'navbar' => '+bars', 
			'navbar-text' => '#fff', 
			'navbar-link-hover' => '+bars', 
			'navbar-link-hover-text' => 'white',
			'container' => 'transparent',
			'content' => 'white', 
			'container-text' => 'black', 
			'header' => 'black', 

			'bars-border' => 'transparent',

			//LISTA DRUŻYN
			'teamlist-name' => 'black', 
			'teamlist-tag' => '+bars', 
			'teamlist-tag-text' => 'white',
			'teamlist-member-summoner-name' => 'rgba(0, 0, 0, .7)', 
			'teamlist-member-summoner-name-text' => 'white',
			'teamlist-member-owner-summoner-name' => 'rgba(200, 140, 0, .7)', 
			'teamlist-member-owner-summoner-name-text' => 'white', 
			'teamlist-join-button-idle' => '+bars', 
			'teamlist-join-button-active' => 'black', 
			'teamlist-request-waiting' => '#f39d13', 
			'teamlist-request-waiting-text' => 'white', 
			'teamlist-request-revoke' => '#d00',
			'teamlist-request-revoke-text' => 'white',
			
			//WIDOK DRUŻYNY
			'teamview-member' => '+bars',
			'teamview-member-text' => 'white',
			'teamview-member-position' => 'white',
			'teamview-member-border' => '+bars',
			'teamview-avatars-empty' => '#eee',
			'teamview-avatars-border' => '+bars',

			'list-separator' => 'transparent', 

			'profile-icon' => '+bars', //ikonki w profilu
			'profile-avatar-border' => '+bars',

			'link' => '#4CCAF0',
			'link-hover' => '#4CCAF0',//'#2CAAD0',

			'input' => 'transparent', //pola formularza
			'input-text' => 'black', 
			'input-disabled' => '#555', //wyłączone pole formularza
			'input-disabled-text' => '#aaa',
			'input-border' => 'gray', //obramowanie pól formularza
			'input-focus-box-shadow' => '0 0 20px rgba(0, 0, 0, .6)',
			'input-focus-border' => 'black',  //obramowanie aktywnego pola formularza
		],

		//FONTY
		'font' => [
			'global' => "'Open Sans'",
			'global-weight' => 300,
			'header' => "'Montserrat'",
			'header-weight' => 400,

			'teamlist-tag' => "''",
			'teamlist-tag-weight' => 300,
			'teamlist-name' => "''",
			'teamlist-name-weight' => 100,
			'teamlist-playercount' => "'Poiret One'",
			'teamlist-playercount-weight' => 700,
			'teamlist-summoner-name' => "''",
			'teamlist-summoner-name-weight' => 300,

			'tooltip' => "''",
			'tooltip-weight' => 400,
		],
	],
];