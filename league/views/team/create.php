<?php

$form = new Form(new Team);

$title = 'Utwórz drużynę';
echo Html::tag('h1', $title);

echo $form->begin([
		'id' => 'signup-form',
		'class' => 'form-wide form-horizontal', 
		'layout' => [
			'label' => 'col-xs-3', 
			'field' => 'col-xs-9', 
			'offset' => 'col-xs-offset-3'
		]
	]);
echo $form->field('name')->text();
echo $form->field('tag')->text();

echo $form->end(true, 'Utwórz drużynę');

?>
