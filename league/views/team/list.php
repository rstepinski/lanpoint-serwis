<?php

$form = new Form(new Team);

$title = 'Lista drużyn';
echo Html::tag('h1', $title
		.(App::loggedIn() && !App::isAdmin() && !(App::$user->team || App::$user->requestedTeam) ? Html::a(Url::to('team/create'), Html::fa('plus').' Utwórz drużynę', ['class' => 'btn btn-info btn-sm team-create'], false) : null)
		.Html::tag('div', Html::fa('toggle-off').' Wyświetl rangi', ['class' => 'display-ranks'], false), [], false);


echo ListView::display([
	'models' => $teams,
	'itemView' => 'team/list/_item',
	'emptyView' => 'team/list/_empty',
	'class' => 'list list-team',
]);

?>
