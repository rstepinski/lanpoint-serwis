
<div class="mailing">
<?php

echo Html::tag('h1', 'Utils');

echo Html::tag('h3', 'Lista mailingowa wszystkich zarejestrowanych użytkowników');
echo Html::tag('div', Html::fa('eye').' Pokaż', ['class' => 'btn btn-info show', 'data-toggle' => '#mailing-all'], false);
echo Html::tag('div', Html::fa('copy').' Skopiuj', ['class' => 'btn btn-warning copy', 'data-copy' => '#mailing-all'], false);
echo Html::tag('textarea', $all, ['id' => 'mailing-all']);

echo Html::tag('h3', 'Lista mailingowa kapitanów wszystkich drużyn');
echo Html::tag('div', Html::fa('eye').' Pokaż', ['class' => 'btn btn-info show', 'data-toggle' => '#mailing-caps'], false);
echo Html::tag('div', Html::fa('copy').' Skopiuj', ['class' => 'btn btn-warning copy', 'data-copy' => '#mailing-caps'], false);
echo Html::tag('textarea', $caps, ['id' => 'mailing-caps']);

echo Html::tag('h3', 'Lista mailingowa kapitanów zablokowanych drużyn');
echo Html::tag('div', Html::fa('eye').' Pokaż', ['class' => 'btn btn-info show', 'data-toggle' => '#mailing-caps-locked'], false);
echo Html::tag('div', Html::fa('copy').' Skopiuj', ['class' => 'btn btn-warning copy', 'data-copy' => '#mailing-caps-locked'], false);
echo Html::tag('textarea', $capsLocked, ['id' => 'mailing-caps-locked']);

echo Html::tag('h3', 'Lista mailingowa użytkowników zablokowanych drużyn');
echo Html::tag('div', Html::fa('eye').' Pokaż', ['class' => 'btn btn-info show', 'data-toggle' => '#mailing-members-locked'], false);
echo Html::tag('div', Html::fa('copy').' Skopiuj', ['class' => 'btn btn-warning copy', 'data-copy' => '#mailing-members-locked'], false);
echo Html::tag('textarea', $membersLocked, ['id' => 'mailing-members-locked']);

?>
</div>

<script type="text/javascript">
	$('.mailing > .btn.show').click(function(){
		$($(this).data('toggle')).slideToggle();
	});
	$('.mailing > .btn.copy').click(function(){
		var pre = document.querySelector($(this).data('copy'));
		pre.select();
		document.execCommand('copy');
	});
</script>