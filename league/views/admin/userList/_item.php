<div class="item">
	<div class="row">
		<div class="col-md-4 name">
			<?= Html::a(Url::to(['user/view', 'id' => $model->id]), $model->fullName, ['class' => 'clear']) ?>
		</div>
		<div class="col-md-2 school">
			<?= $model->school ?>
		</div>
		<div class="col-md-1 team">
			<?= $model->team !== null ? $model->team->tag : null ?>
		</div>
		<div class="col-md-3 summoner">
			<?= $model->isVerified ? Html::fa('check') : ($model->summonerName ? Html::fa('times') : null) ?> <?= $model->summonerName ?> 
		</div>
		<div class="col-md-2 control">
			<?= Html::a('#', Html::fa('trash', true), ['data-href' => Url::to(['admin/removeUser', 'id' => $model->id]), 'data-toggle' => 'modal', 'data-target' => '#confirm'], false); ?>
			<?= Html::a("mailto:$model->email", Html::fa('envelope-o', true), [], false); ?>
		</div>
	</div>
	<hr>
</div>