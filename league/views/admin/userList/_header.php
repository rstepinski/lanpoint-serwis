<div class="item">
	<div class="row header">
		<div class="col-md-4 name">
			<?= Html::a(Url::to(['admin/usersList', 'order' => 'last_name, first_name']), 'Imię i nazwisko', ['class' => 'clear']) ?>
		</div>
		<div class="col-md-2 school">
			<?= Html::a(Url::to(['admin/usersList', 'order' => 'school']), 'Szkoła', ['class' => 'clear']) ?>
		</div>
		<div class="col-md-1 team">
			<?= Html::a(Url::to(['admin/usersList', 'order' => 'team_id']), 'Drużyna', ['class' => 'clear']) ?>
		</div>
		<div class="col-md-3 summoner">
			<?= Html::a(Url::to(['admin/usersList', 'order' => 'summoner_name']), 'Nazwa gracza', ['class' => 'clear']) ?>
		</div>
		<div class="col-md-2 control">
		</div>
	</div>
	<hr>
</div>