<?php

$title = 'Lista użytkowników';
echo Html::tag('h1', $title);

echo ListView::display([
	'models' => $users,
	'itemView' => 'admin/userList/_item',
	'emptyView' => 'admin/userList/_empty',
	'header' => 'admin/userList/_header',
	'class' => 'list list-users',
]);

?>

<div class="modal fade" id="confirm" tabindex="-1" role="dialog" aria-labelledby="label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                Usuwanie użytkownika
            </div>
            <div class="modal-body">
                Czy na pewno chcesz usunąć tego użytkownika?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Anuluj</button>
                <a class="btn btn-danger delete">Niech zdycha</a>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

$('#confirm').on('show.bs.modal', function(e) {
    $(this).find('a.delete').attr('href', $(e.relatedTarget).data('href'));
});

</script>