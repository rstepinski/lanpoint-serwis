<h1>Ustawianie awatara</h1>

<?php

$form = new Form($user);

echo $form->begin([
		'id' => 'user-avatar-form',
		'class' => 'form-wide form-inline', 
		'layout' => [
			'field' => 'col-xs-3', 
		],
		'enctype' => 'multipart/form-data',
	]);
echo $form->field('image')->file(['accept' => 'image/jpeg,image/png']);

echo $form->end(true, 'Zmień awatar');


?>