
<h1>Edycja profilu</h1>

<div class="row profile">
	
	<div class="col-lg-2 mb-3 mb-lg-0">
		
		<div class="avatar">
			
			<img src="<?= $user->avatarPath ?>" class="img-rounded"/>
			
			<div class="avatar-buttons">
			
				<?php if ($user->hasAvatar):

				echo Html::a(Url::to('user/deleteAvatar'), Html::fa('remove'), ['class' => 'btn btn-sm btn-danger'], false);
				echo Html::a(Url::to('user/updateAvatar'), Html::fa('pencil'), ['class' => 'btn btn-sm btn-warning'], false);
				
				else:
					
				echo Html::a(Url::to('user/updateAvatar'), Html::fa('plus'), ['class' => 'btn btn-sm btn-success'], false);

				endif; ?>
				
			</div>
			
		</div>
		
	</div>
	
	<div class="col-lg-10">
		
		<?php $form = new Form($user);
		
		echo $form->begin([
			'id' => 'user-edit-form',
			'class' => 'form-wide form-horizontal', 
			'layout' => [
				'label' => 'col-xs-2', 
				'field' => 'col-xs-10', 
				'offset' => 'col-xs-offset-2'
			]
		]);
		
		echo $form->field('firstName')->text();
		echo $form->field('lastName')->text();
		echo $form->field('school')->dropdown(App::$config['lo_list']);
		
		echo $form->end(true, Html::fa('save').' Zapisz zmiany');
		
		?>
		
		<div class="summoner <?= $user->summonerName ? ($user->isVerified ? '' : 'unverified') : 'inactive'?>"><?= Html::fa('gamepad', true).' '.($user->summonerName 
		? $user->summonerName.Html::a(Url::to(['user/unpair']), Html::fa('unlink'), [
				'class' => 'btn btn-danger btn-fa unpair',
				'data-toggle' => 'tooltip',
				'data-placement' => 'top',
				'title' => 'Rozłącz konto'
			], false).($user->isVerified ? null : Html::a(Url::to(['user/verify']), Html::fa('check'), [
				'class' => 'btn btn-warning btn-fa verify',
				'data-toggle' => 'tooltip',
				'data-placement' => 'top',
				'title' => 'Zweryfikuj konto'
			], false))
		: Html::a(Url::to(['user/pair']), Html::fa('link').' Powiąż z kontem LoL', ['class' => 'btn btn-info btn-sm pair'], false)); ?>
		</div>
		
		<div class="team <?= $user->summonerName ? '' : 'inactive'?>">
			<?= Html::fa('users', true) ?>
			
			<?php			 
				
			if ($user->team)
			{
				if ($user->isMember)
				{
					echo $user->team->name.' ';
				}
				else
				{
					echo $user->team->name.' '.Html::tag('small', '(oczekujące)');
				}
				
				echo Html::a(Url::to(['user/leaveTeam']), Html::fa($user->isMember ? 'sign-out' : 'remove'), [
					'class' => 'btn btn-warning btn-fa '.($user->id == $user->team->ownerId ? 'btn-left' : null),
					'data-toggle' => 'tooltip',
					'data-placement' => 'top',
					'title' => $user->isMember ? 'Opuść drużynę' : 'Cofnij prośbę',
				], false);
				
				if ($user->id == $user->team->ownerId)
				{
					echo Html::a(Url::to(['team/remove', 'id' => $user->team->id]), Html::fa('remove'), [
						'class' => 'btn btn-danger btn-fa btn-right',
						'data-toggle' => 'tooltip',
						'data-placement' => 'top',
						'title' => 'Usuń drużynę'
					], false);
				}
			}
			else
			{
				if ($user->summonerName)
				{
					echo Html::a(Url::to('team/list'), Html::fa('plus').' Dołącz do drużyny', ['class' => 'btn btn-info btn-sm'], false);
				}
				else
				{
					echo 'Nie możesz dołączyć do drużyny, ponieważ nie powiązałeś swojego konta League of Legends.';
				}
			}
				
			?>
		</div>
		
	</div>
	
</div>