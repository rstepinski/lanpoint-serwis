<h1>Parowanie konta</h1>

<?php

$form = new Form($user);

echo $form->begin([
		'id' => 'user-lol-pair-form',
		'class' => 'form-wide form-horizontal', 
		'layout' => [
			'label' => 'col-xs-3', 
			'field' => 'col-xs-9', 
			'offset' => 'col-xs-offset-3'
		]
	]);
echo $form->field('summonerName')->text();

echo $form->end(true, 'Sparuj konto');


?>