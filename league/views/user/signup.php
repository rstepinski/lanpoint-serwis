<?php

$form = new Form(new User);

$title = 'Zarejestruj się';
echo Html::tag('h1', $title);

echo $form->begin([
		'id' => 'signup-form',
		'class' => 'form-wide form-horizontal', 
		'layout' => [
			'label' => 'col-xs-2', 
			'field' => 'col-xs-10', 
			'offset' => 'col-xs-offset-2'
		]
	]);
echo $form->field('username')->text();
echo $form->field('password')->password();
echo $form->field('email')->text();
echo $form->field('firstName')->text();
echo $form->field('lastName')->text();
echo $form->field('school')->dropdown(App::$config['lo_list']);

echo Html::tag('div', 'Zakładając konto oświadczasz jednocześnie, że akceptujesz ogólny regulamin turnieju oraz regulamin rozgrywek League of Legends.', ['class' => 'col-xs-offset-2 terms'], false);

echo $form->end(true, 'Utwórz konto');

?>
