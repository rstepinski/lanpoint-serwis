<h1>Weryfikacja konta LoL</h1>
<div class="verify">
<p>Aby zweryfikować konto League of Legends, zmień nazwę jednej ze swoich stron na runy na kod wyświetlony poniżej. Gdy już to zrobisz, klinij przycisk.</p>
<div class="vcontainer">
	<div class="vkey"><?= $model->verificationKey ?></div>
	<a href="<?= Url::to(['user/verify', 'done' => true]) ?>"><div class="button"><?= Html::fa('check') ?></div></a>
</div>
</div>