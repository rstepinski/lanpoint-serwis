<?php

$form = new Form(new User);

$title = 'Zaloguj się';
echo Html::tag('h1', $title);

echo $form->begin([
		'id' => 'login-form',
		'class' => 'form-horizontal', 
		'layout' => [
			'label' => 'col-xs-2', 
			'field' => 'col-xs-9', 
			'offset' => 'col-xs-offset-2'
		]
	]);
echo $form->field('username')->text();
echo $form->field('password')->password();
echo $form->end(true, 'Zaloguj się');

echo Html::tag('div', 'Nie masz konta? '.Html::a(Url::to('user/signup'), 'Zarejestruj się').' już teraz!', ['class' => 'col-xs-offset-1'], false);

?>


<div class="disclaimer-container">
<div class="disclaimer">
	<div class="header">DISCLAIMER</div>
	LANPoint nie jest wspierany przez Riot Games i nie prezentuje poglądów lub opinii Riot Games lub kogokolwiek oficjalnie związanego
	z produkcją i zarządzaniem League of Legends. League of Legends i Riot Games są znakami towarowymi lub zarejestrowanymi znakami towarowymi Riot Games, Inc. League of Legends © Riot Games, Inc.
</div>
</div>