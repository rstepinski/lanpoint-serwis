<?php

$form = new Form($model);

$title = 'Edytuj artykuł';
echo Html::tag('h1', $title);

echo $form->begin([
		'id' => 'news-form',
		'class' => 'form-wide form-horizontal', 
		'layout' => [
			'field' => 'col-xs-12', 
		]
	]);
echo $form->field('title')->text();
echo $form->field('date')->dateTime();
echo $form->field('content')->textarea();

echo $form->end(true, 'Zapisz');

?>
