<?php

class SiteController extends BaseController
{
	function permissions()
	{
		return [
			'index' => self::ACCESS_GUEST,
			'login' => self::ACCESS_GUEST,
			'about' => self::ACCESS_GUEST,
			'faq' => self::ACCESS_GUEST,
			'terms' => self::ACCESS_GUEST,
		];
	}
	
	function actionLogout()
	{
		App::logout();
		App::addFlash('success', 'Zostałeś wylogowany');
		return App::redirect(Url::to('site/login')); 
	}
	
	function actionLogin()
	{
		$model = new User(User::SCENARIO_LOGIN);
		
		if ($model->load(App::$post))
		{
			if ($model->validate())
			{
				$user = User::find()->where(['username' => $model->username])->one();
				if ($user !== null && $user->login($model->password))
				{
					$user->updateRank();
					App::login($user);
					App::addFlash('success', 'Pomyślnie zalogowano');
				}
				else
				{
					App::addFlash('danger', 'Błędna nazwa użytkownika lub hasło');
				}
			}
			else
			{
				$model->flashErrors();
			}
		}
		
		return $this->render('site/login');
	}
}