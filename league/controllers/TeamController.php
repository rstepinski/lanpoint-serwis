<?php

class TeamController extends BaseController
{
	function permissions()
	{
		return [
			'list' => self::ACCESS_GUEST,
			'view' => self::ACCESS_GUEST,
		];
	}
	
	function actionCreate()
	{
		$model = new Team(Team::SCENARIO_CREATE);
		
		$user = App::$user;
		
		if ($user !== null && $user->summonerName)
		{
			if ($model->load(App::$post))
			{
				if ($model->validate())
				{				
					if (ctype_alnum($model->tag))
					{
						$model->ownerId = App::$user->id;

						if ($model->save(false))
						{
							$user = User::findOne(['id' => App::$user->id]);
							$team = Team::findOne(['name' => $model->name]);
							$user->teamId = $team->id;
							$user->isMember = true;

							if ($user->update(false))
							{
								App::updateUser();
								App::addFlash('success', 'Drużyna została utworzona');
								return App::redirect(Url::to(['team/view', 'id' => $team->id]));
							}
						}

						App::addFlash('danger', 'Nie udało się utworzyć drużyny');
					}
					else
					{
						App::addFlash('danger', 'Tag nie może zawierać znaków specjalnych');
					}
				}
				else
				{
					$model->flashErrors();
				}
			}
		}
		else
		{
			App::addFlash('danger', 'Musisz najpierw powiązać swoje konto.');
			App::goBack();
		}
		
		return $this->render('team/create');
	}
	
	function actionEdit()
	{
		$team = App::$user->team;
		$team->scenario = Team::SCENARIO_EDIT;
		
		if ($team !== null && App::$user->id == $team->ownerId)
		{
			if ($team->load(App::$post))
			{
				if ($team->update())
				{
					App::addFlash('success', "Zmiany zapisano.");
				}
				else
				{
					$team->flashErrors();
				}
				App::updateUser();
			}
			
			return $this->render('team/edit', ['team' => $team]);
		}
		else
		{
			App::addFlash('warning', 'Nie masz uprawnień, aby to zrobić.');
		}
		
		return App::goBack();
	}
	
	function actionRemove($id)
	{
		$team = Team::findOne(['id' => $id]);
		$user = App::$user;
		
		if ($user->id == $team->ownerId)
		{
			if (!$team->isLocked)
			{
				foreach ($team->members as $member)
				{
					$member->isMember = false;
					$member->teamId = null;
					$member->position = User::POSITION_TBD;
					$member->update(false);
				}

				if ($team->delete())
				{
					App::updateUser();
					App::addFlash('success', 'Drużynę usunięto.');
				}
				else
				{
					App::addFlash('danger', 'Nie udało się usunąć drużyny.');
				}
			}
			else
			{
				App::addFlash('danger', 'Drużyna jest zablokowana. Odblokuj ją, by ją usunąć.');
			}
		}
		else
		{
			App::addFlash('warning', 'Nie masz uprawnień, aby to zrobić.');
		}
		
		return App::goBack();
	}
	
	function actionView($id)
	{
		$model = Team::findOne(['id' => $id]);
		
		if ($model !== null)
		{
			return $this->render('team/view', ['model' => $model]);
		}
		
		App::addFlash('warning', 'Drużyna nie istnieje');
		return App::goBack();
	}
	
	function actionList()
	{
		$models = Team::find()->order('name')->run();
		
		return $this->render('team/list', ['teams' => $models]);
	}
	
	function actionJoin($id)
	{
		$user = App::$user;
		$team = Team::findOne(['id' => $id]);
		
		if ($team !== null)
		{
			if ($user->teamId !== null)
			{
				if ($user->isVerified)
				{
					$user->teamId = $id;
					$user->isMember = false;

					if ($user->update(false))
					{
						App::addFlash('success', 'Wysłano prośbę o dołączenie do drużyny');
						App::updateUser();
					}
					else
					{
						App::addFlash('danger', 'Wystąpił błąd');
					}
				}
				else
				{
					App::addFlash('danger', 'Musisz najpierw powiązać i zweryfikować swoje konto League of Legends');
				}
			}
			else
			{
				App::addFlash('warning', 'Wysłałeś już prośbę o dołączenie bądź jesteś już członkiem innej drużyny. Opuść ją lub cofnij prośbę, aby móc dołączyć do tej drużyny');
			}
		}
		else
		{
			App::addFlash('danger', "Nie ma takiej drużyny");
		}
		
		return App::goBack();
	}
	
	function actionAccept($id)
	{
		$user = App::$user;
		if ($user->isMember)
		{
			$team = Team::findOne(['id' => $user->teamId]);
			if ($team->ownerId == $user->id)
			{
				if (!$team->isLocked)
				{
					if (!$team->isFull)
					{
						$request = User::findOne(['id' => $id]);
						if ($request !== null || $request->teamId != $team->id)
						{
							$request->isMember = true;
							$request->position = User::POSITION_TBD;
							$request->update(false);
						}
						else
						{
							App::addFlash('danger', "Użytkownik nie istnieje lub nie wysłał prośby o dołączenie");
						}
					}
					else
					{
						App::addFlash('danger', "Drużyna jest pełna");
					}
				}
				else
				{
					App::addFlash('danger', "Drużyna jest zablokowana, nie możesz tego zrobić");
				}
			}
			else
			{
				App::addFlash('danger', "Nie masz uprawnień, aby to zrobić");
			}
		}
		else
		{
			App::addFlash('danger', "Nie masz uprawnień, aby to zrobić");
		}
		
		
		return App::goBack();
	}
	
	function actionRemoveUser($id)
	{
		$user = App::$user;
		if ($user->isMember)
		{
			$team = Team::findOne(['id' => $user->teamId]);
			if ($team->ownerId == $user->id)
			{
				$request = User::findOne(['id' => $id]);
				if (!$team->isLocked || $request->isMember)
				{
					if ($request !== null && $request->teamId == $team->id)
					{
						$request->isMember = false;
						$request->teamId = null;
						$request->position = User::POSITION_TBD;
						$request->update(false);
					}
					else
					{
						App::addFlash('danger', "Użytkownik nie istnieje lub nie jest członkiem drużyny ani nie wysłał prośby o dołączenie");
					}
				}
				else
				{
					App::addFlash('danger', "Drużyna jest zablokowana, nie możesz tego zrobić");
				}				
			}
			else
			{
				App::addFlash('danger', "Nie masz uprawnień, aby to zrobić");
			}
		}
		else
		{
			App::addFlash('danger', "Nie masz uprawnień, aby to zrobić");
		}
		
		
		return App::goBack();
	}
	
	function actionSelectUserPosition($uid, $pos)
	{
		$user = App::$user;
		if ($user->isMember)
		{
			$team = Team::findOne(['id' => $user->teamId]);
			if ($team->ownerId == $user->id)
			{
				$player = User::findOne(['id' => $uid]);
				if (!$team->isLocked)
				{
					if ($player !== null && $player->isMember)
					{
						$player->position = $pos;
						$player->update(false);
					}
					else
					{
						App::addFlash('danger', "Użytkownik nie istnieje lub nie jest członkiem drużyny");
					}
				}
				else
				{
					App::addFlash('danger', "Drużyna jest zablokowana, nie możesz tego zrobić");
				}				
			}
			else
			{
				App::addFlash('danger', "Nie masz uprawnień, aby to zrobić");
			}
		}
		else
		{
			App::addFlash('danger', "Nie masz uprawnień, aby to zrobić");
		}
		
		return App::goBack();
	}
	
	function actionLock($id)
	{
		$user = App::$user;
		if ($user->team !== null && $user->team->ownerId == $user->id)
		{
			$team = Team::findOne(['id' => $user->team->id]);
			if ($team->isValid)
			{
				$teams = Team::find()->where(['is_locked' => true])->asArray()->run();
				if (count($teams) < App::$config['tournament']['maxEntries'])
				{
					$team->isLocked = true;
					$team->update(false);
					App::addFlash('success', 'Drużyna została zablokowana.');	
				}
				else
				{
					App::addFlash('warning', 'Niestety wszystkie miejsca są już zajęte.');	
				}
			}
			else
			{
				App::addFlash('warning', 'Drużyna nie jest gotowa. Sprawdź, czy na pewno masz pięciu członków i wszystkie pozycje są obsadzone.');	
			}
		}
		else
		{
			App::addFlash('danger', "Nie masz uprawnień, aby to zrobić");
		}
		
		return App::goBack();
	}
	
	function actionUnlock($id)
	{
		$user = App::$user;
		if ($user->team !== null && $user->team->ownerId == $user->id)
		{
			$team = Team::findOne(['id' => $user->team->id]);
			$team->isLocked = false;
			$team->update(false);
		}
		else
		{
			App::addFlash('danger', "Nie masz uprawnień, aby to zrobić");
		}
		
		return App::goBack();
	}
}