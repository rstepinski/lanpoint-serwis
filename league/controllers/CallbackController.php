<?php

class CallbackController extends BaseController
{
	function permissions()
	{
		return [
			'matchData' => self::ACCESS_GUEST,
		];
	}
	
	function actionMatchData()
	{
		$matchData = json_decode(App::$post);
	}
}