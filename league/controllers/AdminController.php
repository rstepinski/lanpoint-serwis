<?php

class AdminController extends BaseController
{
	function onConstruct()
	{
		$this->defaultAccessLevel = self::ACCESS_ADMIN;
	}
			
	function permissions()
	{
		return [];
	}
	
	function actionUsersList($order = null)
	{
		$users = User::find()->where("id <> '".App::$user->id."'");
		if ($order !== null)
		{
			$users->order($order);
		}
		$users = $users->asArray()->run();
		
		return $this->render('admin/userList', ['users' => $users]);
	}
	
	function actionRemoveUser($id)
	{
		$user = User::findOne(['id' => $id]);
		
		if ($user !== null)
		{
			$user->delete();
			App::addFlash('success', 'Użytkownik usunięty.');
		}
		else
		{
			App::addFlash('danger', 'Użytkownik nie istnieje.');
		}
		
		return App::goBack();
	}
	
	function actionRemoveTeam($id)
	{
		$team = Team::findOne(['id' => $id]);
		
		if ($team !== null)
		{
			foreach ($team->members as $member)
			{
				$member->isMember = false;
				$member->teamId = null;
				$member->position = User::POSITION_TBD;
				$member->update(false);
			}
			
			$team->delete();
			App::addFlash('success', 'Drużyna usunięta.');
		}
		else
		{
			App::addFlash('danger', 'Drużyna nie istnieje.');
		}
		
		return App::redirect(Url::to(['team/list']));
	}
	
	function actionSeed($a = 'list', $teams = null)
	{
		switch ($a)
		{
			case 'list':
				$teams = Team::find()->where(['is_locked' => true])->asArray()->run();

				foreach ($teams as $team)
				{
					$ranks = [];
					foreach ($team->members as $member)
					{
						if ($member->rank >= 0) { $ranks[] = $member->rank; }
						else { $ranks[] = TournamentHelper::GOLD; }
					}			

					sort($ranks);

					$diff = $ranks[4] - $ranks[3];

					$ranks[4] -= $diff / 2; 

					if (count($ranks == 5)) { array_splice($ranks, 0, 1); }  

					$mean = ($ranks[0] + $ranks[1] + $ranks[2] + $ranks[3]) / 4;

					$team->rating = $mean;
				}

				$max = App::$config['tournament']['maxEntries'];

				usort($teams, function($a, $b){ return $a->rating - $b->rating; } );

				return $this->render('team/list', ['teams' => $teams]);
		}
	}
	
	function actionUtils()
	{
		$allModels = (new Query)->select('email')->from('user')->asArray()->run();
		
		$all = "";
		foreach ($allModels as $m)
		{
			$all .= $m['email'].",\n";
		}
		
		$teams = Team::find()->asArray()->run();
		
		$caps = "";
		$capsLocked = "";
		$membersLocked = "";
		
		foreach ($teams as $team)
		{
			foreach ($team->members as $member)
			{
				if ($member->id == $team->ownerId)
				{
					if ($team->isLocked)
					{
						$capsLocked .= $member->email.",\n";
					}
					$caps .= $member->email.",\n";
				}
				
				if ($team->isLocked)
				{
					$membersLocked .= $member->email.",\n";
				}
			}
		}
		
		return $this->render('admin/utils', ['all' => $all, 'caps' => $caps, 'capsLocked' => $capsLocked, 'membersLocked' => $membersLocked]);
	}
}