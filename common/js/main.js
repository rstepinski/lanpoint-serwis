$(document).ready(function(){
    $('.close').click(function(){$(this).parent('.dismiss').parent('.callout').fadeOut(300)});
    
    $('[data-toggle="tooltip"]').tooltip();
    
    
    $('.list-team .member').mouseenter(function(){
        $(this).find('.overlay').fadeIn({duration: 200});
    });
    $('.list-team .member').mouseleave(function(){
        $(this).find('.overlay').fadeOut({duration: 200});
    });
    $('.list-team .member.join').mouseenter(function(){
        $(this).addClass('hover');
    });
    $('.list-team .member.join').mouseleave(function(){
        $(this).removeClass('hover');
    });
    
    
    $('.list-team .member.waiting').mouseenter(function(){
        $(this).addClass('hover');
        $(this).find('a').css('visibility', 'hidden');
        $(this).find('a').html('<i class="fa fa-fw fa-times"></i><span>COFNIJ</span>');
        $(this).find('a').css('visibility', 'visible');
    });
    $('.list-team .member.waiting').mouseleave(function(){
        $(this).removeClass('hover');
        $(this).find('a').css('visibility', 'hidden');
        $(this).find('a').html('<i class="fa fa-fw fa-clock-o"></i><span>OCZEKUJĄCE</span>');
        $(this).find('a').css('visibility', 'visible');
    });
    
    $('.display-ranks').click(function(){
        if($('.display-ranks .fa').hasClass('fa-toggle-off'))
        {
            $('.list-team .members .member .rank-underlay').fadeIn('fast');
        }
        else
        {
            $('.list-team .members .member .rank-underlay').fadeOut('fast');
        }
            
        $('.display-ranks .fa').toggleClass('fa-toggle-off');
        $('.display-ranks .fa').toggleClass('fa-toggle-on');
    });
    
    $('.edit-users').click(toggleEditUsers);
    $('.edit-positions').click(toggleEditPositions);
    
    function toggleEditUsers()
    {
        if($('.edit-users .fa').hasClass('fa-toggle-off'))
        {
            $('.team-view .members .member .delete').addClass('visible');
            if($('.edit-positions .fa').hasClass('fa-toggle-on')) toggleEditPositions();
        }
        else
        {
            $('.team-view .members .member .delete').removeClass('visible');
        }
            
        $('.edit-users .fa').toggleClass('fa-toggle-off');
        $('.edit-users .fa').toggleClass('fa-toggle-on');
    }
    function toggleEditPositions()
    {
        if($('.edit-positions .fa').hasClass('fa-toggle-off'))
        {
            $('.team-view .members .member .info > div.pos-select').fadeIn();
            $('.team-view .members .member .info .name').fadeOut();
            if($('.edit-users .fa').hasClass('fa-toggle-on')) toggleEditUsers();
        }
        else
        {
            $('.team-view .members .member .info > div.pos-select').fadeOut();
            $('.team-view .members .member .info .name').fadeIn();
        }
            
        $('.edit-positions .fa').toggleClass('fa-toggle-off');
        $('.edit-positions .fa').toggleClass('fa-toggle-on');
    }
    
    
    $('.list-team .teaminfo').mouseenter(function(){
        $(this).parents('.item').find('.members .member .rank-underlay').fadeIn('fast');
    });
    $('.list-team .teaminfo').mouseleave(function(){
        if ($('.display-ranks .fa').hasClass('fa-toggle-off'))
        {
            $(this).parents('.item').find('.members .member .rank-underlay').fadeOut('fast');
        }
    });
});
