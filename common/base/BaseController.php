<?php

abstract class BaseController extends BaseClass
{
	const ACCESS_GUEST = 0b00;
	const ACCESS_USER = 0b01;
	const ACCESS_ADMIN = 0b10 | self::ACCESS_USER;
    
    public $action = null;
	
	protected $defaultAccessLevel = self::ACCESS_USER;
	
	function __construct()
	{
		$this->onConstruct();
	}
	
	function onConstruct() {}
	
	function beforeAction($name)
	{
		$hasPermissions = isset($this->permissions()[$name]);
		$accessLevel = $hasPermissions ? $this->permissions()[$name] : $this->defaultAccessLevel;
		
		$userAccessLevel = App::getAccessLevel();
		
		return ($accessLevel & $userAccessLevel) == $accessLevel;
	}
	
	function permissions()
	{
		return [];
	}
    
    function getName()
    {
        return substr(lcfirst(self::shortName()), 0, -10);
    }
	
	function __call($name, $arguments)
	{
        App::$controller = $this;
        
		$method = 'action'.ucfirst($name);
		
		$rclass = new ReflectionClass($this);
		
		$hasAction = $rclass->hasMethod($method);
				
		if (!$hasAction)
		{
			throw new NotFoundException("Action ".$method." does not exist in controller ".$this->shortName());
		}
		elseif ($this->beforeAction($name))
		{
			$reflect = new ReflectionMethod($this, $method);
			
			foreach ($reflect->getParameters() as $param)
			{
				if (!isset($arguments[$param->getName()]))
				{
					if (isset($_GET[$param->getName()]))
					{
						$arguments[$param->getName()] = $_GET[$param->getName()];
					}
					elseif (isset(App::$post[$param->getName()]))
					{
						$arguments[$param->getName()] = App::$post[$param->getName()];
					}
					elseif (!$param->isOptional())
					{
						throw new Exception("Missing argument ".$param->getName()." in ".$this->shortName()."->".$method."()");
					}
				}
			}
			
            $this->action = $name;
			
			call_user_func_array([$this, $method], $arguments);
		}
		else 
		{ 
			App::addFlash('danger', 'Musisz się zalogować');
			App::redirect(Url::to('site/login'));
		}
	}
	
	function render($view, $params = [], $layout = true)
	{
		$_variables = "";
		foreach ($params as $key => $v)
		{
			$_variables .= '$'.$key.' = $params["'.$key.'"]; ';
		}
		
		eval($_variables);
		
		$viewPath = VIEWS.$view.'.php';
		
		if (!file_exists($viewPath))
		{
			trigger_error("View file <b>$view</b> does not exist.", E_USER_ERROR);
		}
		
		ob_start();
		include VIEWS.$view.'.php';
		$content = ob_get_contents();
		ob_end_clean();
		
		if ($layout)
		{
			include(VIEWS.'layouts/main.php');
		}
		else
		{
			echo $content;
		}
	}
}
