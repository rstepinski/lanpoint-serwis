<?php

//WARNING: This class assumes you have underscore_naming in your database and camelCase as object properties.

/**
 * Base class for every model. It has basic methods for database operations, 
 * such as <i>find()</i> and <i>findOne()</i>.<br/>It allows user to create various <i>getFoo()</i> 
 * methods, which can then be accessed as regular object properties, ie. <i>$obj->foo</i> 
 * instead of <i>$obj->getFoo()</i> and setting them in exactly the same way.<br/>
 * It also provides a <i>load()</i> method, which is useful when loading form data
 * to object. Simply use <i>$obj->load(App::$post)</i> to do this.<br/>It also provides
 * validation specified by <i>rules()</i> method and saving the model to the database
 * with or without validation.<br/>It provides basic form error control.
 * @author Radosław Stępiński
 */
abstract class ActiveRecord extends BaseClass implements ArrayAccess
{
	protected $properties = [];
	protected $errors = [];
	public $scenario;
	
	const CLEAR = [];
	
	function __construct($scenario = null)
	{
		$this->scenario = $scenario; 
	}
		
	function __get($name)
	{
		if (property_exists($this, $name))
		{
			return $this->$name;
		}
		else
		{
			if (isset($this->properties[$name]))
			{
				return $this->properties[$name];
			}
			
			if (method_exists($this, "get".ucfirst($name)))
			{
				return call_user_func_array([$this, "get".ucfirst($name)], []);
			}
			
			if ($name == "tableName")
			{
				return String::toUnderscore($this->shortName()); 
			}
            
            if ($name == "attributeLabels")
            {
                return call_user_func_array([$this, $name], []);
            }
			
			if (isset($this->id) && self::findOne(['id' => $this->id]) == null) { return null; }
			
			if (isset($this->properties[$name]))
			{
				return $this->properties[$name];
			}
			else { return null; }
		}
	}
	
	function __set($k, $v)
	{
		if (property_exists($this, $k))
		{
			$this->$k = $v;
		}
		else 
		{
			$this->properties[$k] = $v;
		}
	}
	
	/**
	 * initializes object values from an associative array
	 * @param array $array key => value pairs of object data
	 */
	function initValues($array)
	{
		foreach ($array as $k => $v)
		{
			$name = String::toCamelCase($k);
			$this->$name = $v;
		}
	}
	
	/**
	 * Constructs a query to select items from the database
	 * @return Query constructed query with table name and select command specified
	 */
	static function find()
	{
		$query = (new Query(self::shortName()))->select()
			->from(self::tableName());
		   
		return $query;
	}
	
	/**
	 * Gets first model from the database matching passed condition
	 * @param string|array $condition condition being checked
	 * @return ActiveRecord|null object from the database if it was found, <b>null</b> otherwise.
	 */
	static function findOne($condition)
	{
		$result = (new Query(self::shortName()))->select()
			->from(self::tableName())
			->where($condition)
			->one();
		
		if ($result == false)
		{
			return null;
		}
		
		return $result;
	}
	
	/**
	 * @return string Model table in database
	 */
	static function tableName()
	{
		return String::toUnderscore(self::shortName());
	}
	
	function offsetExists($o)
	{
		return $this->$o !== null;
	}
	
	function offsetGet($o)
	{
		return $this->$o;
	}
	
	function offsetSet($o, $v)
	{
		$this->$o = $v;
	}
	
	function offsetUnset($o)
	{
		unset ($this->$o);
	}
	
	/**
	 * Defines attribute labels for model used in form creation
	 * @return array Attribute labels
	 */
	function attributeLabels()
	{
		return [];
	}
	
	/**
	 * Defines validation rules for the model
	 * @return array Validation ruless
	 */
	function rules()
	{
		return [];
	}
	
	/**
	 * Loads associative array to the model
	 * @param array $array Associative array of model properties in following manner:
	 * <i>Class:property => value</i>, ie. <i>User:username => foobar</i>
	 * @return bool Whether loading was succesful
	 */
	function load($array)
	{	
		$result = false; 
		
		foreach ($array as $key => $value)
		{
			if (strpos($key, $this->shortName().':') !== false)
			{
				$property = preg_filter('#.*?\:(.*?)#i', '$1', $key);
				$this->$property = $value;
				$result = true;
			}
		}
		
		return $result;
	}
	
	/**
	 * Saves the model to the database
	 * @param bool $runValidation whether to run validation before saving, defaults to <b>true</b>
	 * @return bool Whether the operation was succesful
	 */
	function save($runValidation = true)
	{
		$query = (new Query)->insert($this->tableName, $this->properties);
		if ($runValidation)
		{
			if ($this->validate())
			{
				return $query->run();
			}
			return false;
		}
		else
		{
			return $query->run();
		}
	}
	
	function update($runValidation = true)
	{
		$query = (new Query)->update($this->tableName, $this->properties)->where(['id' => $this->id]);
		
		if ($runValidation)
		{
			if ($this->validate())
			{
				return $query->run();
			}
			return false;
		}
		else
		{
			return $query->run();
		}
	}
	
	function delete()
	{
		return (new Query)->delete($this->tableName)->where(['id' => $this->id])->run();
	}
	
	/**
	 * Manually appends an error to the model.
	 * @param string $field Field to which the error will be appended
	 * @param string $error Error message
	 */
	function addError($field, $error)
	{
		$this->errors[$field] = $error;
	}
	
	/**
	 * Same as <i>hasErrors()</i>, but checks errors for specified field.
	 * @param string $field Name of field being checked
	 * @return bool <b>true</b> if errors are present, <b>false</b> otherwise.
	 */
	function hasError($field)
	{
		return isset($this->errors[$field]); 
	}
	
	/**
	 * Tells you if model has any errors - put by user or <i>validate()</i> method
	 * @return bool <b>true</b> if errors are present, <b>false</b> otherwise.
	 */
	function hasErrors()
	{
		return !empty($this->errors);
	}
	
	/**
	 * Used to validate model properties
	 * @return bool <b>true</b> if no errors were encountered, <b>false</b> otherwise.
	 */
	function validate()
	{
		$this->sanitize();
		 
		$result = true;
		foreach ($this->properties as $property => $value)
		{
			$error = $this->validateField($property, $value);
			$error = preg_filter(
				"#\{\{field\}\}#i", 
				Html::tag('b', lcfirst(isset($this->attributeLabels()[$property]) 
					? $this->attributeLabels()[$property] 
					: String::toWords($property)), []), 
				$error
			);
			if ($error !== null)
			{
				$result = false;
				$this->addError($property, $error);
			}
		}
		
		return !$this->hasErrors();
	}
	
	/**
	 * Used to validate given form field
	 * @param string $property name of the field being validated
	 * @param mixed $value value of said field
	 * @return string|null <b>null</b> if no errors are encountered, else <b>string</b>
	 * containing error message.
	 */
	function validateField($property, $value)
	{
		$validators = [];
		
		if (empty($this->rules()))
		{
			return null;
		}
		
		foreach ($this->rules() as $key => $_validators)
		{
			if ($key == $property)
			{
				foreach ($_validators as $validator)
				{
					$array = gettype($validator) == "array";
					$class = $array ? $validator[0] : $validator;
					if ($array) { unset($validator[0]); }
					
					$validators[] = [
						'class' => $class, 
						'className' => ucfirst($class).'Validator', 
						'methods' => $array ? $validator : ['_none_' => 'validate']
					];
				}
			}
		}
		
		if (!$validators)
		{
			return null;
		}
		
		foreach ($validators as $e)
		{
			$validator = new $e['className']($value, $this->tableName());
			
			if (isset($e['methods']['msg']))
			{
				$validator->message = $e['methods']['msg'];
				unset($e['methods']['msg']);
			}
			
			foreach ($e['methods'] as $method => $arg)
			{
				if ($method == '_none_')
				{
					if (!call_user_func([$validator, $arg], []))
					{
						return $validator->error;
					}
				}
				elseif (!call_user_func([$validator, $method], $arg))
				{
					return $validator->error;
				}
			}
		}
		
		return null;
	}
	
	/**
	 *  Used to convert form errors to app's flash message.
	 *  @return void
	 */
	function flashErrors()
	{
		$errors = "";
		
		if (count($this->errors) > 1)
		{
			foreach ($this->errors as $error)
			{
				$errors .= Html::tag('li', $error, [], false);
			}

			App::addFlash('danger', Html::tag('ul', $errors, [], false));
		}
		else
		{
			foreach ($this->errors as $error)
			{
				App::addFlash('danger', $error);
			}
		}
	}
	
	/**
	 *  This gets called every time validation is run, to prevent form injection hacks.
	 *  Utilizes CLEAR constant. 
	 */
	function sanitize()
	{
		foreach (self::CLEAR as $variable)
		{
			if (isset($this->$variable)) { unset($this->$variable); }
		}
	}
}