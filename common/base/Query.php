<?php

class Query
{
	private $q = "";
	private $class = null;
	
	private $command = null;
	private $columns = null;
	private $values = null;
	private $table = null;
	private $where = null;
	private $limit = null;
	private $offset = null;
	private $order = null;
	private $update = null;
	
	private $safe = false;
	
	private $asArray = false;
	
	function __construct($class = null)
	{
		$this->class = $class;
	}
	
	function asArray()
	{
		$this->asArray = true;
		
		return $this;
	}
	
	function select($select = null)
	{
		$this->command = "SELECT";
		
		if (gettype($select) == "array")
		{
			$this->columns .= "(";
			
			$first = true;
			
			foreach ($select as $column)
			{
				if (!$first) { $this->columns .= ", "; }
				$this->columns .= $column;
				$first = false;
			}
			
			$this->columns .= ")";
		}
		else if ($select != null)
		{
			$this->columns .= $select;
		}
		else
		{
			$this->columns .= "*";
		}
		
		return $this;
	}
	
	function setSafe()
	{
		$this->safe = true;
		return $this;
	}
	
	function update($table, $update)
	{
		$this->command .= "UPDATE";
		
		$this->update = $update;
		
		$this->table = $table;
		
		return $this;
	}
	
	function insert($table, $insert)
	{
		$this->command .= "INSERT";
		
		$cols = [];
		$values = [];
		
		foreach ($insert as $k => $v)
		{
			$cols[] = String::toUnderscore($k);
			$values[] = $v;
		}
		
		for ($i = 0; $i < sizeof($cols); $i++)
		{
			if ($i != 0) { $this->columns .= ", "; }
			$this->columns .= $cols[$i];
		}
		
		for ($i = 0; $i < sizeof($values); $i++)
		{
			if ($i != 0) { $this->values .= ", "; }
			$this->values .= "'".App::$database->real_escape_string($values[$i])."'";
		}
		
		$this->table = $table;
		
		return $this;
	}
	
	function delete($table)
	{
		$this->command = 'DELETE';
		$this->table = $table;
		
		return $this;
	}
	
	function from($from)
	{
		$this->table = $from;
		return $this;
	}
	
	function where($where)
	{		
		if (gettype($where) == "array")
		{
			$i = 0;
			
			foreach($where as $k => $v)
			{
				$this->where .= "$k = '".App::$database->real_escape_string($v)."'";
						 $i++;
				if ($i != sizeof($where)) { $this->where .= " AND "; } 
			}
		}
		else
		{
			$this->where .= $where;
		}
		
		return $this;
	}
	
	function orWhere($where)
	{
		$this->where .= " OR ";
		return $this->where($where);
	}
	
	function andWhere($where)
	{
		$this->where .= " AND ";
		return $this->where($where);
	}
	
	function limit($limit)
	{
		$this->limit .= $limit; 
		return $this;
	}
	
	function offset($offset)
	{
		$this->offset .= $offset;
		return $this;
	}
	
	function order($order)
	{
		if (gettype($order) == "array")
		{
			if (gettype($order[0]) == "array")
			{
				foreach ($order as $o)
				{
					$this->order = $o[0]." ".$o[1];
				}
			}
			else
			{
				$this->order = $order[0]." ".$order[1];
			}
		}
		else
		{
			$this->order .= $order;
		}
		
		return $this;
	}
	
	function one()
	{
		$this->limit = 1;
		return $this->run();
	}
	
	function run()
	{
		$result = App::$database->query($this->parse());
		
		if (gettype($result) == "boolean")
		{
			return $result;
		}
		else
		{
			$array = [];
			
			while ($row = $result->fetch_assoc())
			{
				if ($this->class !== null)
				{
					$reflect = new ReflectionClass($this->class);
					$obj = $reflect->newInstanceWithoutConstructor();
					$obj->initValues($row);
					
					$array[] = $obj;
				}
				else
				{
					$array[] = $row;
				}
			}			
			
			if ($this->asArray) 
			{
				return $array;
			}
			return count($array) == 0 ? null : (count($array) > 1 ? $array : $array[0]);
		}
	}
	
	function parse()
	{
		$query = "";
		
		switch ($this->command)
		{
			case 'SELECT': $query .= $this->command.' '.$this->columns.' FROM '.$this->table; break;
			case 'INSERT': $query .= $this->command.' INTO '.$this->table.' ('.$this->columns.') VALUES ('.$this->values.')'; break;
			case 'UPDATE': $query .= $this->command.' '.$this->table.' SET '.$this->parseKV($this->update); break;
			case 'DELETE': $query .= $this->command.' FROM '.$this->table;
		}
		
		$query .= ($this->where ? ' WHERE '.$this->where : null)
			.($this->limit ? ' LIMIT '.$this->limit : null)
			.($this->offset ? ' OFFSET '.$this->offset : null)
			.($this->order ? ' ORDER BY '.$this->order : null);
		
		return $query;
	}
	
	function parseKV($array)
	{		
		$first = true;
		$return = "";
		
		foreach($array as $k => $v)
		{
			$k = String::toUnderscore($k);
			if (!$first) { $return .= ", "; }
			$return .= "$k='".($this->safe ? $v : App::$database->real_escape_string($v))."'";
			$first = false;
		}
		
		return $return;
	}
}
