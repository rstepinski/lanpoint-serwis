<?php

Class Nav
{
	public static function display($params)
	{
		$id = 'navbar';
		if (isset($params['id'])) { $id = $params['id']; }
		
		$menu = '';
		
		foreach($params['items'] as $item)
		{
			if (!isset($item['condition']) || $item['condition'])
			{
				$menu .= Html::tag('li', Html::a($item['location'], $item['label'], isset($item['class']) ? ['class' => $item['class'].' nav-link'] : ['class' => 'nav-link'], false), ['class' => 'nav-item'], false);
			}
		}
		
		$menu = Html::tag('ul', $menu, ['class' => 'navbar-nav nav ml-auto'], false);
		
		return Html::tag('div', $menu, ['id' => 'navbarSupportedContent', 'class' => 'collapse navbar-collapse'], false);
	}
}

