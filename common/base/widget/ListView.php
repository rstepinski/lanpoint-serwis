<?php

Class ListView
{
	public static function display($params)
	{
		$id = 'list';
		$class = 'list';
		if (isset($params['id'])) { $id = $params['id']; }
		if (isset($params['class'])) { $class = $params['class']; }
		
		$content = '';
		
		if ($params['models'] !== null)
		{
			if (!is_array($params['models'])) { $params['models'] = [$params['models']]; }
			
			if (isset($params['header']))
			{
				ob_start();
				App::$controller->render($params['header'], [], false);
				$content .= ob_get_contents();
				ob_end_clean();
			}
			
			foreach($params['models'] as $model)
			{
				ob_start();
				App::$controller->render($params['itemView'], ['model' => $model], false);
				$content .= ob_get_contents();
				ob_end_clean();
			}
		}
		else
		{
			App::$controller->render($params['emptyView'], [], false);
		}
		return Html::tag('div', $content, ['id' => $id, 'class' => $class], false);
	}
}

