<?php

class Callout
{
	static function show($style, $message, $dismissable = true)
	{
		$icon = null;
		switch ($style)
		{
			case 'danger': $icon = 'exclamation-circle'; break;
			case 'warning': $icon = 'exclamation-triangle'; break;
			case 'success': $icon = 'check'; break;
			case 'info': $icon = 'info-circle'; break;
			default: break;
		}
		$icon = $icon ? Html::fa($icon) : null;
		$close = '<button type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
		return Html::tag('div', 
				Html::tag('div', $icon, ['class' => 'icon'], false)
				.Html::tag('div', $message, ['class' => 'message'], false)
				.($dismissable ? Html::tag('div', $close, ['class' => 'dismiss'], false) : null), 
			['class' => "callout callout-$style bg-$style text-$style"], false);
	}
}