<?php

class App	
{
	public static $database;
	public static $config;
	public static $styleConfig;
    public static $controller;
	public static $user = null;
	public static $post = [];
	public static $flash = null;
	public static $referer = null;
	
	private static $service;
	
	const APPSTATE_IDLE = 0;
	const APPSTATE_ELIMINATION = 1;
	const APPSTATE_GROUP_1 = 2;
	const APPSTATE_GROUP_2 = 3;
	const APPSTATE_FINALS = 4;
	
	const SERVICE_NEWS = 0;
	const SERVICE_LEAGUE = 1;
	const SERVICE_CSTRIKE = 2;
	
	const DB_INIT = false;
	
	const RELOAD_RCSS = 0;
	
	static function init()
	{
		self::$config = require('config/config.php');
		self::$service = self::$config['app']['service'];
		
		self::$referer[0] = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : Url::to('site/index');
		self::$referer[1] = isset($_SESSION['referer']) ? $_SESSION['referer'] : Url::to('site/index');
		
		$_SESSION['referer'] = self::$referer[0];
		
		self::$user = isset($_SESSION['user-'.self::$service]) ? $_SESSION['user-'.self::$service] : null;
		
		if (self::RELOAD_RCSS) { self::$styleConfig = require('config/style.php'); }
		
		self::$database = new mysqli(self::$config['db']['host'], self::$config['db']['user'], self::$config['db']['pass'], self::$config['db']['db']);
		self::$database->set_charset('utf8');
		
		self::$post = $_POST;
		
		self::RCSS();
	}
	
	static function goBack($steps = 0)
	{
		self::redirect(self::$referer[$steps]);
	}
	
	static function login($user)
	{
		self::$user = $user;
		$_SESSION['user-'.self::$service] = self::$user;
	}
	
	static function logout()
	{
		self::$user = null;
		$_SESSION['user-'.self::$service] = null;
	}
	
	static function redirect($where)
	{
		header("Location: $where");
	}
	
	static function hasFlash()
	{
		self::$flash = isset($_SESSION['flash']) ? $_SESSION['flash'] : null;
		return (bool) self::$flash;
	}
	
	static function getFlash()
	{
		self::$flash = isset($_SESSION['flash']) ? $_SESSION['flash'] : null;
		
		if (self::hasFlash())
		{
			array_reverse(self::$flash);
			$e = array_pop(self::$flash);
			array_reverse(self::$flash);
			
			$_SESSION['flash'] = self::$flash;
			
			return $e;
		}
		return null;
	}
	
	static function getAllFlashes()
	{
		self::$flash = isset($_SESSION['flash']) ? $_SESSION['flash'] : null;	
		$_SESSION['flash'] = null;
		return self::$flash;
	}
	
	static function addFlash($type, $message)
	{
		self::$flash[] = ['type' => $type, 'message' => $message];
		$_SESSION['flash'] = self::$flash;
	}
	
	static function loggedIn()
	{
		return self::$user !== null;
	}
	
	static function isAdmin()
	{
		return self::loggedIn() && self::getAccessLevel() == BaseController::ACCESS_ADMIN;
	}
	
	static function getAccessLevel()
	{
		if (self::$user === null || !self::loggedIn()) 
		{ 
			return BaseController::ACCESS_GUEST; 
		}
		else
		{
			if (self::$user->role == User::ROLE_USER) { return BaseController::ACCESS_USER; }
			if (self::$user->role == User::ROLE_ADMIN) { return BaseController::ACCESS_ADMIN; }
		}
	}
	
	static function updateUser()
	{
		$newUser = User::findOne(['id' => self::$user->id]);
		self::login($newUser);
	}
	
	static function fileUpload($field, $options = [])
	{
		//error_reporting(0);
		
		$maxSize = isset($options['maxSize']) ? $options['maxSize'] : App::$config['fileUpload']['maxSize']; 
		$mimeType = isset($options['mimeType']) ? $options['mimeType'] : App::$config['fileUpload']['mimeType']; 
		$path = App::$config['fileUpload']['path'];
		
		if (!isset($_FILES[$field]['error']) || is_array($_FILES[$field]['error'])) 
		{
			App::addFlash('danger', "Wystąpił błąd.");
		}
		else
		{
			$result = false;
			switch ($_FILES[$field]['error']) 
			{
				case UPLOAD_ERR_OK:
					$result = true; break;
				case UPLOAD_ERR_NO_FILE:
					App::addFlash('danger', "Nie wysłano żadnego pliku.");
				case UPLOAD_ERR_INI_SIZE:
				case UPLOAD_ERR_FORM_SIZE:
					App::addFlash('danger', 'Przekroczono limit rozmiaru pliku.');
				default:
					App::addFlash('danger', 'Wystąpił nieznany błąd.');
			}

			if ($result)
			{

				if ($_FILES[$field]['size'] > $maxSize)
				{
					App::addFlash('danger', 'Przekroczono limit rozmiaru pliku.');
				}
				else
				{
					$finfo = new finfo(FILEINFO_MIME_TYPE);
					if (false === $ext = array_search(
						$finfo->file($_FILES[$field]['tmp_name']),
						$mimeType,
						true
					)) 
					{
						App::addFlash('danger', 'Niepoprawny format pliku.');
					}
					else
					{
						$file = sprintf($path.'%s.%s', sha1_file($_FILES[$field]['tmp_name']), $ext);
						
						if (!move_uploaded_file($_FILES[$field]['tmp_name'], $file)) 
						{
							App::addFlash('danger', 'Nie udało się przenieść pliku.');
						}
						else
						{
							App::addFlash('success', 'Plik został załadowany.');
							
							error_reporting(E_ALL);
							
							return $file;
						}
					}
				}
			}
		}
							
		error_reporting(E_ALL);
		
		return false;
	}
	
	static function i18n($text, $file)
	{
		$arr = require ('i18n/'.$file.'.php');
		
		foreach ($arr as $from => $to)
		{
			$text = str_replace($from, $to, $text);
		}
		return $text;
	}
	
	static function dump($args, $die = false)
	{
		if (!is_array($args))
		{
			$args = [$args];
		}
		
		echo '<pre>';
		foreach ($args as $arg)
		{
			var_dump($arg);
		}
		echo '</pre>';
		
		if ($die) { die; }
	}
	
	static function lolAPI($call, $data = null)
	{
		$baseUrl = 'https://eune.api.pvp.net/api/lol/eune/';
		$key = '?api_key='.self::$config['api_key'];
		
		$enc = preg_replace('#\s#', '', $data);
		
		$text = '';
		switch ($call)
		{
			case 'summoner-name': $text = "v1.4/summoner/by-name/$enc"; break;
			case 'summoner': $text = "v1.4/summoner/$enc"; break;
			case 'runes': $text = "v1.4/summoner/$enc/runes"; break;
			case 'league-entry': $text = "v2.5/league/by-summoner/$enc/entry"; break;
			default: return null;
		}
		
		$url = $baseUrl.$text.$key;
		
		$responseCode = substr(get_headers($url)[0], 9, 3);
		
		if ($responseCode == 200)
		{
			$result = @file_get_contents($url);
			$object = json_decode($result);
			$_data = strtolower(preg_replace('#\s#', '', $data));
			return $object->$_data;
		}
		else if ($responseCode == 404)
		{
			return $responseCode;
		}
		else if ($responseCode >= 400)
		{
			App::addFlash('danger', 'Nie udało się połączyć z serwerami Riot Games. Spróbuj ponownie później.');
		}
		return $responseCode;
	}
	
	static function RCSS()
	{
		if (self::RELOAD_RCSS)
		{
			$files = ['main'];
			
			foreach ($files as $file)
			{
				$css = file_get_contents(App::$config['rcss']['path'].'/'.$file.'.rcss');

				$styleConfig = App::$styleConfig[$file];
				
				if (isset($styleConfig['_GFONT-IMPORT']))
				{
					$fonts = $styleConfig['_GFONT-IMPORT'];
					unset($styleConfig['_GFONT-IMPORT']);

					$fonts = preg_replace('#(\s)#', '+', $fonts);
					$fonts = is_array($fonts) ? $fonts : [$fonts];

					foreach ($fonts as $font)
					{
						$css = "@import url(https://fonts.googleapis.com/css?family=$font:100,300,400,500,700);" . $css;
					}
				}

				if (isset($styleConfig['_DEFINE']))
				{
					$defs = $styleConfig['_DEFINE'];
					unset($styleConfig['_DEFINE']);

					foreach ($styleConfig as $type => $flags)
					{
						foreach ($flags as $flag => $value)
						{
							foreach ($defs as $def => $v)
							{
								if ($value == '+'.$def)
								{
									$styleConfig[$type][$flag] = $v;
								}
							}
						}
					}
				}

				foreach ($styleConfig as $type => $flags)
				{
					foreach ($flags as $flag => $value)
					{
						$css = preg_replace('#\{('.$type.'-'.$flag.')\}#', $value, $css);
					}
				}

				(new Query)->setSafe()->update('appstate', ['style_hash' => md5(json_encode(App::$styleConfig))])->run();

				file_put_contents(App::$config['rcss']['path'].'/generated/'.$file.'.css', $css);
			}
		}
	}
	
	const ERR_API_USER_NOT_FOUND = 1;
	const ERR_API_ID_INCORRECT = 2;
	const ERR_USER_VAC = 9;
	const ERR_USER_BLACKLISTED = 10;
	
	static function valveAPI($id)
	{
		$url = "https://steamid.eu/api/request.php?api=".self::$config['api_key']."&player=".$id;
		$data = simplexml_load_file($url);
		
		if (isset($data->error))
		{
			return (int)$data->error->errorid;
		}
		
		if ($data->profile_status->vac != 0)
		{
			return self::ERR_USER_VAC;
		}
		if ($data->profile_status->blacklisted != 0)
		{
			return self::ERR_USER_BLACKLISTED;
		}
			
		return $data;
	}
}
