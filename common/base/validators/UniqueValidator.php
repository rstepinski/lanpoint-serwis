<?php

class UniqueValidator extends BaseValidator
{
	function field($field)
	{
		$this->msg();
		return (new Query)->select()->from($this->table)->where([$field => $this->content])->run() === null;
	}
	
    function validate()
    {
		$this->msg(); 
        return true;
    }
}

