<?php

/**
 * @description Class used to validate a variety of number-related 
 * @author Radosław Stępiński
 */
class NumberValidator extends BaseValidator
{
    function min($min)
    {
		$this->msg();
        return $this->returnIt($this->content >= $min);
    }
    
    function max($max)
    {
		$this->msg();
        return $this->returnIt($this->content <= $max);
    }
    
    function integer()
    {
		$this->msg();
        return $this->returnIt(is_int($this->content));
    }
    
    function validate()
    {
		$this->msg();
        return $this->returnIt(is_numeric($this->content));
    }
}
