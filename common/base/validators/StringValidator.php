<?php

/**
 * @description Class used to validate a variety of string-related conditions
 * @author Radosław Stępiński
 */
class StringValidator extends BaseValidator
{
    function length($length)
    {
        $min = isset($length['min']) ? $length['min'] : null;
        $max = isset($length['max']) ? $length['max'] : null;
		
		$this->msg("Pole {{field}} musi mieć "
				.($min !== null ? "min. $min znaków" : "")
				.($min !== null && $max !== null ? " i " : "")
				.($max !== null ? "max. $max znaków" : ""));
        
        return ($min === null || (strlen($this->content) >= $min)) 
            && ($max === null || (strlen($this->content) <= $max));
    }
    
    function pattern($pattern)
    {
		$this->msg();
        return (bool) preg_match($pattern, $this->content);
    }
    
    function validate()
    {
		$this->msg("Pole {{field}} musi być ciągiem znaków");
        return is_string($this->content);
    }
}
