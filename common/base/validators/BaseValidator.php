<?php

/**
 * @class BaseValidator 
 * @description Base validator class. Defines default message when encountering an error.
 * Every validator must extend this class.
 * @author Radosław Stępiński
 */
class BaseValidator
{
	const DEFAULT_MSG = 'Błąd w polu {{field}}';
	
    protected $content = null;
	public $message = null;
	protected $table = null;
	public $error = null;
    
    function __construct($content, $table)
    {
        $this->content = $content; 
		$this->table = $table;
    }  
	
	function msg($msg = null)
	{
		if (!$msg) { $msg = self::DEFAULT_MSG; }
		$this->error = $this->message !== null ? $this->message : $msg;
	}
}
