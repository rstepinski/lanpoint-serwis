<?php

/**
 * @description Class used to verify if passed form field is not empty
 * @author Radosław Stępiński
 */
class RequiredValidator extends BaseValidator
{
    function validate()
    {
		$this->msg("Pole {{field}} nie może być puste");
        return $this->content != null;
    }
}
