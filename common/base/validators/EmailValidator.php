<?php

/**
 * @description Validator class used to validate if passed argument is an email.
 * @author Radosław Stępiński
 */
class EmailValidator extends BaseValidator
{
	/**
	 * @var REGEX defines regular expression which will be matched against
	 * during validation
	 */
    const REGEX = '#.*?\@.*?\..*?#i';
    
    function validate()
    {
		$this->msg(); 
        return preg_match(self::REGEX, $this->content);
    }
}

