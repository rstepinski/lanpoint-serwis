<?php

include 'App.php';
include 'BaseClass.php';
include 'BaseController.php';
include 'ErrorController.php';

register_shutdown_function("error_handler");
set_error_handler(error_handler());
error_reporting(0);

function error_handler() 
{
	$errfile = "unknown file";
	$errstr  = "shutdown";
	$errno   = E_CORE_ERROR;
	$errline = 0;

	$error = error_get_last();

	if ($error !== null) 
	{
		$errno   = $error["type"];
		$errfile = $error["file"];
		$errline = $error["line"];
		$errstr  = $error["message"];

		if (DEBUG_MODE == DEBUG_DEV)
		{
			format_error($errno, $errstr, $errfile, $errline);
		}
		else
		{
			$errCtrl = new ErrorController;
			if (strpos($errstr, 'NotFoundException'))
			{
				$errCtrl->render('error/404');
			}
			else
			{
				$errCtrl->render('error/404');
			}
		}
	}
}

function format_error( $errno, $errstr, $errfile, $errline ) {
	ob_start();
	var_dump(debug_backtrace(false), true);
	$trace = ob_get_contents();
	ob_end_clean();

	$type = 'Error';
	switch ($errno)
	{
		case E_RECOVERABLE_ERROR:
		case E_USER_ERROR:
		case E_ERROR: $type = 'Fatal Error'; break;
		
		case E_CORE_ERROR: $type = 'Core Error'; break;
		
		case E_PARSE: $type = 'Parse Error'; break;
		
		case E_USER_NOTICE:
		case E_NOTICE: $type = 'Notice'; break;
		
		case E_CORE_WARNING:
		case E_USER_WARNING:
		case E_WARNING: $type = 'Warning'; break;
		
		case E_DEPRECATED:
		case E_USER_DEPRECATED: $type = 'Deprecated Method'; break;
	}
	
	$content = '<div class="error-header">PHP '.$type.':</div>'
		.'<div class="error-info">'.$errstr.'</div>'
		.'<div class="error-file">on line '.$errline.' in file '.$errfile.'</div>';
	
	echo $content;
}