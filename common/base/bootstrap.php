<?php

$classesDir = [
    BASE,
	MODELS,
	CONTROLLERS,
	VIEWS,
	VENDOR,
];

function loadConfig()
{
	foreach (glob("config/*.php") as $filename)
	{
		include $filename;
	}
}

function __autoload($class_name) 
{
	global $classesDir;
	foreach ($classesDir as $directory) 
	{
		if (load($directory, $class_name))
		{
			return true;
		}
	}
}

function load($dir, $class_name)
{	
	if (file_exists($dir . $class_name . '.php')) 
	{
		require_once ($dir . $class_name . '.php');
		return true;
	}
	else
	{
		$list = scandir($dir);
		foreach ($list as $file)
		{
			if (is_dir($dir.$file) && $file != '.' && $file != '..')
			{
				if (load($dir.$file.'/', $class_name)) { return true; }
			}
		}
	}
}