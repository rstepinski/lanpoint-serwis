<?php

abstract class DateTimeHelper
{
	static function format($datetime, $format)
	{
		if (!is_numeric($datetime))
		{
			$datetime = strtotime($datetime);
		}
		return App::i18n(date($format, $datetime), 'months-II');
	}
	
	static function getSeconds($amount, $time = 'days')
	{
		$i = 1;
		switch ($time)
		{
			case 'years': $i *= 12;
			case 'months': $i *= 30;
			case 'days': $i *= 24;
			case 'hours': $i *= 60;
			case 'minutes': $i *= 60;
			default: $i = $i;
		}
		
		return $amount * $i;
	}
	
	static function subtract($from, $amount, $type = 'days')
	{
		if (!is_numeric($from))
		{
			$from = strtotime($from);
		}
		return $from - self::getSeconds($amount, $type);
	}
	
	static function add($to, $amount, $type = 'days')
	{
		if (!is_numeric($to))
		{
			$to = strtotime($to);
		}
		return $to + self::getSeconds($amount, $type);
	}
	
	public static function compare($date1, $date2 = null)
	{
		$date2 = $date2 ?: time();
		$date1 = is_numeric($date1) ? $date1 : strtotime($date1);
		$date2 = is_numeric($date2) ? $date2 : strtotime($date2);
		return $date1 - $date2;
	}
	
	public static function compareDate($date1, $date2 = null)
	{
		$date2 = $date2 ?: time();
		$date1 = is_numeric($date1) ? $date1 : strtotime($date1);
		$date2 = is_numeric($date2) ? $date2 : strtotime($date2);
		return strtotime(date('Y-m-d', $date1)) - strtotime(date('Y-m-d', $date2));
	}
}


