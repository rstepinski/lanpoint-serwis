<?php

abstract class Url
{
	static function to($action)
	{
		$get = "";
		
		$params = [];
		if (gettype($action) == "array")
		{
			$_temp = $action[0];
			unset($action[0]);
			$params = $action;
			$action = $_temp;
		}

		$first = true;
		foreach($params as $k => $v)
		{
			$get .= ($first ? "?" : "&").urlencode($k)."=".urlencode($v);
			$first = false;
		}
		
		return "//".HOST."/".$action.$get;
	}
	
	static function img($src, $c = true)
	{
		if ($c) { return COMMONS.'/img/'.$src; }
		return BASEDIR.'/site/img/'.$src;
	}
	
	static function file($src, $c = true)
	{
		if ($c) { return COMMONS.'/files/'.$src; }
		return BASEDIR.'/site/files/'.$src;
	}
}