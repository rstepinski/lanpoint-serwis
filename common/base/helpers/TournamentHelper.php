<?php

abstract class TournamentHelper
{
	const BRONZE = 0;
	const SILVER = 270;
	const GOLD = 625;
	const PLATINUM = 1540;
	const DIAMOND = 5800;
	const MASTER = 16000;
	const CHALLENGER = 32000;
	
	static function exportTeamsToCSV()
	{
		$teams = Team::find()->where(['is_locked' => 1])->run();

		if (!$teams) { echo"No teams found.\n"; }
		
		$handler = fopen('C:/xampp/htdocs/gorion/output/csv/teams.csv', 'w+');
		if (!$handler) { echo"Handler error.\n"; }
		
		$f = fputcsv($handler, ['id', 'name', 'tag', 'avgRank'], ';');
		if (!$f) { echo"fputcsv error.\n"; }
		
		$teams = is_array($teams) ? $teams : [$teams];
		
		foreach ($teams as $team)
		{
			$avgRank = 0;
			$count = 5;
			foreach ($team->members as $member)
			{
				$rank = self::calculateRank($member);
				if ($rank == 9999) { $count--; }
				else { $avgRank += $rank; }
			}
			$avgRank /= $count;
			$avgRank = round($avgRank);
			
			fputcsv($handler, [$team->id, $team->name, $team->tag, $avgRank], ';');
		}
		
		fclose($handler);
	}
	
	static function calculateRank($player)
	{
		$division = true;
		$nextTier;
		
		switch ($player->tier)
		{
			case 'challenger': $division = false; break;
			case 'master': $division = false; break;
			case 'provisional': $division = false; return -1; break;
			case 'bronze': $nextTier = "SILVER"; break;
			case 'silver': $nextTier = "GOLD"; break;
			case 'gold': $nextTier = "PLATINUM"; break;
			case 'platinum': $nextTier = "DIAMOND"; break;
			case 'diamond': $nextTier = "MASTER"; break;
			default: break;
		}
		
		$tier = strtoupper($player->tier);
		
		$eval = '$rank = self::'.$tier.';';
		
		$div = 0;
	
		switch ($player->division)
		{
			case 'i': $div = 4; break;
			case 'ii': $div = 3; break;
			case 'iii': $div = 2; break;
			case 'iv': $div = 1; break;
			case 'v': $div = 0; break;
		}

		if ($division)
		{
			$eval = '$rank = self::'.$tier.' + (self::'.$nextTier.' / 5 * '.$div.');';
		}
		
		eval($eval);
		
		return $rank;
	}
	
	static function tournamentCode()
	{
		
	}
}
