<?php

class String
{
	/**
	 * Converts camelCase to underscore
	 * @param string $camelCase camelCase string
	 * @return string thatsAnExample -> thats_an_example
	 */
	static function toUnderscore($camelCase)
	{
		$arr = str_split($camelCase);

		$return = "";
		
		$first = true;
		foreach($arr as $l)
		{
			$return .= ctype_upper($l) ? ($first ? "" : "_").strtolower($l) : $l;
			$first = false;
		}

		return $return;
	}
	
	/**
	 * Converts underscore to camelCase
	 * @param string $camelCase camelCase string
	 * @param bool $ucfirst Whether to make first letter capital. Defaults to false.
	 * @return string thats_an_example -> thatsAnExample (or ThatsAnExample if $ucfirst is true)
	 */
	static function toCamelCase($underscore, $ucfirst = false)
	{
		$arr = str_split($underscore);
		
		$return = "";
		
		$nextUpper = false;
		$first = true;
		
		foreach($arr as $l)
		{
			if ($l == "_") { $nextUpper = true; continue; }
			$return .= $nextUpper || ($first && $ucfirst) ? ucfirst($l) : $l;
			$nextUpper = false;
			$first = false;
		}
		
		return $return;
	}
	
	/**
	 * Converts camelCase to words
	 * @param string $camel camelCase string
	 * @param bool $ucfirst Whether to make first letter capital. Defaults to true.
	 * @return string thatsAnExample -> Thats An Example (or "thats An Example" if $ucfirst is false)
	 */
    static function toWords($camel, $ucfirst = true)
    {
        $arr = str_split($camel);
        
        $return = "";
        
        foreach($arr as $l)
        {
            $return .= ctype_upper($l) ? " $l" : $l;
        }
        
        return $ucfirst ? ucfirst($return) : $return;
    }
	
	static function truncate($string, $limit = 100)
	{
		return strlen($string) > $limit ? substr($string, 0, $limit).'...' : $string;
	}
}