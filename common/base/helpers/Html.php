<?php

abstract class Html
{
	const SINGLE_TAGS = ['br', 'img', 'meta', 'link'];
	
	static function a($href, $text = null, $options = [], $clean = true)
	{
		$options['href'] = $href;
		return Html::tag('a', $text, $options, $clean);
	}
    
    static function field($type, $options = [])
    {
        $params = self::stringify($options);
		
        return '<input '.$params.' type="'.$type.'" />';
    }
    
    static function label($for, $val, $options = [])
    {
		$options['class'] = isset($options['class']) ? $options['class'].' control-label' : 'control-label';
		$options['for'] = $for;
        return self::tag('label', $val, $options, false);
	}
	
	static function fa($icon, $fw = false)
	{
		return Html::tag('i', null, ['class' => 'fa fa-'.$icon.' '.($fw?'fa-fw':null)]);
	}
	
	/**
	 * Generates HTML tag using specified content and options.
	 * @param string $tag The tag itself
	 * @param string $content Contents of the tag
	 * @param array|string $options HTML attributes of the tag
	 * @param boolean $clean Whether to encode HTML chars or not
	 * @return string String containing the tag.
	 */
	static function tag($tag, $content = null, $options = [], $clean = true)
	{		
		$content = ($clean ? htmlspecialchars($content) : $content);
		$params = gettype($options) == "array" ? self::stringify($options) : $options;
		
		if (array_search($tag, self::SINGLE_TAGS)) { return "<$tag $params />"; }
		
		return "<$tag $params>$content</$tag>";
	}
            
	/**
	 * Stringifies an array for use with HTML tags.
	 * @param array $array Array to stringify
	 * @return string String containing key-value pairs in such manner: key="value" key2="value2" ...
	 */
    static function stringify($array)
    {
        $params = "";
		foreach ($array as $k => $v)
		{
			$params .= $k.'="'.$v.'" ';
		}
        
        return $params;
    }
}