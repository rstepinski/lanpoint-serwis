<?php

abstract class BaseClass
{
	static function shortName()
	{
		$reflect = new ReflectionClass(self::className());
		return $reflect->getShortName();
	}
	
	static function className()
	{
		$backtrace = debug_backtrace();
        $static = $backtrace[1]['type'] == '::';
		
		return $static ? get_called_class() : get_class($this);
	}
}