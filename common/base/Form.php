<?php

class Form
{
    public $model = null;
	public $layout = null;
	
	public $inline = false;
    
    function __construct($model)
    {
        $this->model = $model;
    }    
    
    function begin($options = [])
    {
		if (isset($options['inline']))
		{
			$this->inline = true;
			unset($options['inline']);
		}
		
		if (isset($options['layout']))
		{
			$this->layout = $options['layout'];
			unset($options['layout']);
		}
		
        $url = Url::to(isset($options['action']) ? $options['action'] : App::$controller->getName()."/".App::$controller->action);
		unset($options['action']);
        
        return '<form action="'.$url.'" '.(isset($options['method']) ? null : 'method="post"').Html::stringify($options).'>';
    }
   
    function end($submit = true, $text = 'Submit')
    {
		return Html::tag('div', Html::tag('button', $text, ['type' => 'submit', 'class' => 'btn btn-success '.(isset($this->layout['offset']) ? $this->layout['offset'] : null)], false), ['class' => 'form-group'], false).'</form>';
    }
    
    function field($property, $options = [])
    {
        return new FormField($property, $this, $options);
    }
}