<?php

class UserController extends BaseController
{
	function permissions()
	{
	}
	
	function actionCreate()
	{
		$model = new User;
		
		if ($model->load(App::$post))
		{
			if ($model->validate())
			{
				$model->password = password_hash($model->password, PASSWORD_DEFAULT);
				
				if ($model->save(false))
				{
					App::addFlash('success', 'Admion hast been gibt');
					return App::redirect(Url::to('site/login'));
				}
				
				App::addFlash('danger', 'Nie udało się utworzyć konta');
			}
			else
			{
				$model->flashErrors();
			}
		}
		
		return $this->render('user/create');
	}
}