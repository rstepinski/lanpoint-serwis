<?php

class NewsController extends BaseController
{
	function permissions()
	{
	}
	
	function actionCreate()
	{
		$model = new News;
		$model->scenario = News::SCENARIO_CREATE;
		
		if ($model->load(App::$post))
		{
			$model->date = DateTimeHelper::format(time(), 'Y-m-d H:i:s');
			if ($model->save())
			{
				App::addFlash('success', 'Artykuł dodano.');
			}
			else
			{
				$model->flashErrors();
			}
		}
		
		return $this->render('news/create', ['model' => $model]);
	}
	
	function actionRemove($id)
	{
		$model = News::findOne(['id' => $id]);
			
		if ($model->delete())
		{
			App::addFlash('success', "Artykuł usunięto");
		}
		else
		{
			App::addFlash('danger', "Nie udało się usunąć artykułu");
		}
		
		return App::goBack();
	}
	
	function actionEdit($id)
	{
		$model = News::findOne(['id' => $id]);
		$model->scenario = News::SCENARIO_CREATE;

		if ($model->load(App::$post))
		{
			if ($model->update())
			{
				App::addFlash('success', "Zmiany zapisano.");
			}
			else
			{
				$model->flashErrors();
			}
		}
		
		return $this->render('news/edit', ['model' => $model]);
	}
}