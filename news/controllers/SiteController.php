<?php

class SiteController extends BaseController
{
	function permissions()
	{
		return [
			'index' => self::ACCESS_GUEST,
			'login' => self::ACCESS_GUEST,
			'about' => self::ACCESS_GUEST,
			'faq' => self::ACCESS_GUEST,
			'terms' => self::ACCESS_GUEST,
			'contact' => self::ACCESS_GUEST,
		];
	}
	
	function actionIndex()
	{
		return $this->render('site/index', ['news' => News::find()->asArray()->order(['date', 'DESC'])->run()]);
	}
	
	function actionLogout()
	{
		App::logout();
		App::addFlash('success', 'Zostałeś wylogowany');
		return App::redirect(Url::to('site/login')); 
	}
	
	function actionLogin()
	{
		$model = new User(User::SCENARIO_LOGIN);
		
		if ($model->load(App::$post))
		{
			if ($model->validate())
			{
				$user = User::find()->where(['username' => $model->username])->one();
				if ($user !== null && $user->login($model->password))
				{
					$user->updateRank();
					App::login($user);
					App::addFlash('success', 'Pomyślnie zalogowano');
					return App::goBack(1);
				}
				else
				{
					App::addFlash('danger', 'Błędna nazwa użytkownika lub hasło');
				}
			}
			else
			{
				$model->flashErrors();
			}
		}
		
		return $this->render('site/login');
	}
	
	function actionAbout()
	{
		return $this->render('site/about');
	}
	
	function actionContact()
	{
		return $this->render('site/contact');
	}
	
	function actionTerms()
	{
		return $this->render('site/terms');
	}
	
	function actionFaq()
	{
		return $this->render('site/faq');
	}
}