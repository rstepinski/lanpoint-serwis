<?php

class News extends ActiveRecord
{
	const SCENARIO_CREATE = 'create';
	
	const CLEAR = ['ownerId'];
	
	function attributeLabels()
	{
		return [
			'title' => 'Tytuł',
			'content' => 'Treść',
		];
	}
	
	function rules()
	{
		switch ($this->scenario)
		{
			case self::SCENARIO_CREATE: return [
				'title' => [
					['required'], 
					['string', 'length' => ['min' => 3, 'max' => 80]], 
				],
				'content' => [['required']],
			];
		}
	}
	
	function getOwner()
	{
		return User::findOne($this->ownerId);
	}
}