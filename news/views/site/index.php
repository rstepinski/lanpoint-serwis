<?php foreach($news as $new): ?>

<div class="news">
	<h1><?= $new->title ?><div class="news-date"><?= DateTimeHelper::format($new->date, 'j F Y H:i') ?></div></h1>
	<?= $new->content ?>
</div>

<?php endforeach; ?>