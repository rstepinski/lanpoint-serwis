<?php

$form = new Form(new User);

$title = 'Zaloguj się';
echo Html::tag('h1', $title);

echo $form->begin([
		'id' => 'login-form',
		'class' => 'form-wide form-horizontal', 
		'layout' => [
			'label' => 'col-xs-2', 
			'field' => 'col-xs-9', 
			'offset' => 'col-xs-offset-2'
		]
	]);
echo $form->field('username')->text();
echo $form->field('password')->password();
echo $form->end(true, 'Zaloguj się');

?>
