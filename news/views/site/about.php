<?php

$title = "Informacje o turnieju";
echo Html::tag('h1', $title);

?>

<p>Turniej e-sportowy LANPoint 2017 jest pierwszym konkursem tego typu organizowanym na tak dużą skalę.
	Do udziału zapraszamy wszystkich uczniów liceów i techników znajdujących się na terenie Wrocławia.
	Zagramy w League of Legends oraz Counter-Strike.
	Chcesz zmierzyć się z rówieśnikami w profesjonalnej atmosferze? Dobrze trafiłeś!
</p>
<p>Rejestracja otwarta!</p>
<p> <?= Html::a(Url::file('regulamin.pdf'), 'Regulamin turnieju'); ?> </p>
<p>Regulamin rozgrywek poszczególnych gier zostanie udostępniony wkrótce. </p>