<?php

$form = new Form(new News(News::SCENARIO_CREATE));

$title = 'Utwórz artykuł';
echo Html::tag('h1', $title);

echo $form->begin([
		'id' => 'news-form',
		'class' => 'form-wide form-horizontal', 
		'layout' => [
			'field' => 'col-xs-12', 
		]
	]);
echo $form->field('title')->text();
echo $form->field('content')->textarea();

echo $form->end(true, 'Zapisz');

?>
