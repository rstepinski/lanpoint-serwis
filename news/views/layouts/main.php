<!DOCTYPE html>
<html>
	
	<head>
		
		<title>LANPoint</title>
		
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		
		<link href='https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,400italic,900,900italic' rel='stylesheet' type='text/css'>
		
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
		<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
		
		<link rel="stylesheet" href="<?= COMMONS ?>/css/bootstrap/font-awesome.css" />
		<link rel="stylesheet" href="<?= COMMONS ?>/css/generated/main.css" />
		
		<script src="//cloud.tinymce.com/stable/tinymce.min.js"></script>
		<script>tinymce.init({ 
			selector:'textarea',
			height: 250,
			menubar: false,
			plugins: [
				'advlist autolink lists link image charmap print preview anchor',
				'searchreplace visualblocks code fullscreen',
				'insertdatetime media table contextmenu paste code'
			],
			toolbar: 'undo redo | insert | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image'});</script>
		
	</head>
	
	<body>
		
		<nav class="navbar navbar-toggleable-md">

			<a class="navbar-brand" href="<?= Url::to('site/index'); ?>"><img src="<?= Url::img('lanpoint_w_sm.png');?>" class="logo"/></a>
			<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<?= Html::fa('bars'); ?>
			</button>
			
			<?php if (App::loggedIn()): ?><div class="admin-label">ADMINISTRATOR</div><?php endif; ?>				
				

			<?php echo Nav::display(['items' => [
				[
					'label' => Html::fa('plus-circle').' Dodaj newsa', 
					'location' => Url::to('news/create'), 
					'condition' => App::loggedIn(),
				],
				[
					'label' => Html::fa('info-circle').' Informacje', 
					'location' => Url::to('site/about'), 
				],
				[
					'label' => Html::fa('phone').' Kontakt', 
					'location' => Url::to('site/contact'), 
				],
				[
					'label' => Html::fa('sign-in').' Zaloguj się', 
					'location' => '//localhost/gorion/landing/landing.html', 
					'condition' => !App::loggedIn(),
				],
				[
					'label' => Html::fa('sign-out').' Wyloguj się', 
					'location' => Url::to('site/logout'), 
					'condition' => App::loggedIn(),
				],
			]]); ?>
				
		</nav>

		<div class="container news">
			
			<div class="content">
			
				<?php if (App::hasFlash()): ?>

				<div class="callouts">

					<?php 
					foreach (App::getAllFlashes() as $flash)
					{
						echo Callout::show($flash['type'], $flash['message']);
					}
					?>

				</div>

				<?php endif;?>
				
				<?= $content ?>
				
			</div>
			
		</div>
		
		<div id="push"></div>
		
		<footer>
			<div class="footer-content">
				&copy;2016 Radosław Stępiński
			</div>
		</footer>
			
		<script type="text/javascript" src="<?= COMMONS ?>/js/main.js"></script>
		
	</body>
	
</html>