<?php			

define("HOST", 'localhost/gorion/news');
define("ROOTPATH", dirname(__FILE__));
define("BASEDIR", substr(dirname(__FILE__), 15));
define("VIEWS", ROOTPATH.'/views/');
define("MODELS", ROOTPATH.'/models/');
define("CONTROLLERS", ROOTPATH.'/controllers/');
define("COMMONS", '//localhost/gorion/common');

define("BASECOMMONS", 'C:/xampp/htdocs/gorion/common');
define("BASE", BASECOMMONS.'/base/');
define("VENDOR", BASECOMMONS.'/vendor/');

const DEBUG_PRODUCTION = 0;
const DEBUG_DEV = 1;
define("DEBUG_MODE", DEBUG_DEV);

include(BASE."bootstrap.php");
include(BASE."errorhandler.php");

App::init();

$reference = isset($_GET['r']) 
	? explode("/", $_GET['r']) 
	: [
		App::$config['app']['defaultController'], 
		App::$config['app']['defaultAction']
	];
$cname = ucfirst($reference[0]).'Controller';
$aname = isset($reference[1]) ? str_replace(';', '', $reference[1]) : App::$config['app']['defaultAction'];

session_start();

App::init();

call_user_func_array([new $cname, $aname], []);
