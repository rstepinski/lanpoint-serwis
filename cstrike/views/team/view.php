
<div class="row team-view cstrike">
	
	<div class="col-sm-12" style="min-width: 768px !important;">
		
		<h1 class="name"><?= $model->name ?></h1>
		
		<div class="tag"><?= $model->tag ?></div>
		
		<div class="member-avatars">
			
			<?php foreach ($model->members as $member): ?>

			<div class="avatar"><img src="<?= $member->avatarPath ?>" /></div>

			<?php endforeach; ?>
			
		</div>
		
		<div class="switches">
			<?php if (App::loggedIn() && $model->ownerId == App::$user->id) { 
				
				if ($model->isLocked) 
				{ 
					echo Html::tag('div', Html::a(Url::to(['team/unlock', 'id' => $model->id]), Html::fa('unlock').' Odblokuj drużynę', ['class' => 'btn btn-sm btn-warning'], false), ['class' => 'lock-team'], false); 
				}
				else 
				{ 
					echo Html::tag('div', Html::fa('toggle-off').' Usuń graczy', ['class' => 'edit-users'], false); 
					echo Html::tag('div', Html::fa('toggle-off').' Edytuj pozycje', ['class' => 'edit-positions'], false);
					
					echo Html::tag('div', Html::a(Url::to(['team/lock', 'id' => $model->id]), Html::fa('lock').' Zablokuj drużynę', ['class' => 'btn btn-sm btn-success '.($model->isValid ? '' : 'disabled')], false), ['class' => 'lock-team'], false); 
					echo Html::tag('div', Html::a(Url::to(['team/edit', 'id' => $model->id]), Html::fa('pencil').' Edytuj drużynę', ['class' => 'btn btn-sm btn-warning'], false), ['class' => 'edit-team'], false); 
				}
			} 
			if (App::isAdmin())
			{
				echo Html::a(Url::to(['admin/removeTeam', 'id' => $team->id]), Html::fa('remove').' Usuń drużynę', ['class' => 'btn btn-danger btn-sm'], false);
			} ?>
		</div>
			
		<div class="members col-sm-12 col-md-9" style="min-width: 460px !important;">
			
			<?php foreach ($model->members as $member): ?>

			<div class="member">
				<div class="info">
					<div class="name"><?= Html::a(Url::to(['user/view', 'id' => $member->id]), "$member->firstName $member->lastName") ?></div>
					<?php if ($model->owner->id != $member->id): ?>
						<div class="delete"><?= Html::a(Url::to(['team/removeUser', 'id' => $member->id]), Html::fa('times-circle'), ['class' => 'remove-user'], false); ?></div>
					<?php endif; ?>
					<div class="summoner"><?= $member->steamName ?></div>
				</div>
			</div>

			<?php endforeach; ?>
			<?php if (App::loggedIn() && App::$user->team !== null && App::$user->team->ownerId == App::$user->id): foreach ($model->requests as $member): ?>

			<div class="member request">
				<div class="info">
					<div class="buttons">
						<?= Html::a(Url::to(['team/accept', 'id' => $member->id]), Html::fa('check'), ['class' => 'accept'], false); ?>
						<?= Html::a(Url::to(['team/removeUser', 'id' => $member->id]), Html::fa('times'), ['class' => 'decline'], false); ?>
					</div>
					<div class="name"><?= "$member->firstName $member->lastName" ?></div>
					<div class="summoner"><?= $member->steamName ?></div>
				</div>
			</div>

			<?php endforeach; endif; ?>
			
		</div>
		
	</div>
	
</div>


<div class="modal fade" id="confirm" tabindex="-1" role="dialog" aria-labelledby="label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                Blokowanie drużyny
            </div>
            <div class="modal-body">
                <p>Pamiętaj, że blokada drużyny oznacza gotowość do udziału w turnieju. Po zamknięciu rejestracji nie będzie można już cofnąć blokady, a więc i edytować składu drużyny.</p>
				<p>Jeśli jeszcze nie zapoznałeś się z regulaminem rozgrywek, zrób to teraz.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Anuluj</button>
                <a class="btn btn-success commit">Zablokuj</a>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">

$('#confirm').on('show.bs.modal', function(e) {
    $(this).find('a.commit').attr('href', $(e.relatedTarget).data('href'));
});
</script>