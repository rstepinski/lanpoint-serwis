
<h1>Edycja drużyny</h1>

<div class="row team-view">
	
	<div class="col-sm-9">
		
		<?php $form = new Form($team);
		
		echo $form->begin([
			'id' => 'team-edit-form',
			'class' => 'form-wide form-horizontal', 
			'layout' => [
				'label' => 'col-xs-2', 
				'field' => 'col-xs-5', 
				'offset' => 'col-xs-offset-2'
			]
		]);
		
		echo $form->field('name')->text();
		echo $form->field('tag')->text();
		
		echo $form->end(true, Html::fa('save').' Zapisz zmiany');
		
		?>		
		
	</div>
	
</div>