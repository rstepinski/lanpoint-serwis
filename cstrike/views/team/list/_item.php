<div class="item">
	<div class="row">
		<div class="col-xs-5 teaminfo">
			<div class="tag"><?= $model->tag ?></div>
			<div class="name"><?= Html::a(Url::to(['team/view', 'id' => $model->id]), $model->name) ?></div>
			<div class="playercount <?= $model->isLocked ? 'locked' : ($model->isValid ? 'full' : null) ?>" <?php if ($model->isLocked): ?>data-toggle="tooltip" data-placement="top" title="Ta drużyna jest zablokowana" <?php endif;?>>
				<?= !$model->isLocked ? ($model->isValid ? 'gotowa' : count($model->members).' / 5') : Html::fa('lock')?>
			</div>
		</div>
		<div class="col-xs-7 members">
			<div class="pull-right">
				<?php if (!$model->isValid && App::loggedIn() && !(App::$user->team || (App::$user->requestedTeam && App::$user->teamId != $model->id))): ?>
			
				<div class="member <?= App::$user->teamId ? 'waiting' : 'join' ?>">
					<a href="<?= Url::to([(App::$user->teamId == $model->id ? 'user/leaveTeam' : 'team/join'), 'id' => $model->id]) ?>">
						<?= App::$user->teamId == $model->id ? Html::fa('clock-o', true).'<span>OCZEKUJĄCE</span>' : Html::fa('user-plus', true); ?>
					</a>
				</div>
				
				<?php endif; ?>
				<?php foreach($model->members as $member): 
					
				$slen = strlen($member->summonerName);	
				$additionalClass = $slen > 14 ? 'smallest' : ($slen > 12 ? 'smaller' : ($slen > 10 ? 'small' : null));
					
				?>

				<a href="<?= Url::to(['user/view', 'id' => $member->id]) ?>">
					<div class="member <?= $member->id == $model->ownerId ? 'owner' : null ?>">
						<img class="avatar" src="<?= $member->avatarPath ?>" />
						<div class="overlay <?= $additionalClass ?>">
							<?= $member->summonerName ?>
						</div>
					</div>
				</a>

				<?php endforeach; ?>
			</div>
		</div>
	</div>
</div>