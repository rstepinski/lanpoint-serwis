<html>
	
	<head>
		
		<title>LANPoint CS</title>
		
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		
		<link href='https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,400italic,900,900italic' rel='stylesheet' type='text/css'>
		
		
		<script type="text/javascript" src="<?= COMMONS ?>/js/jquery.js"></script>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
		<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
		
		<script type="text/javascript" src="<?= COMMONS ?>/js/main.js"></script>
		
		<link rel="stylesheet" href="<?= COMMONS ?>/css/bootstrap/font-awesome.css" />
		<link rel="stylesheet" href="<?= COMMONS ?>/css/generated/main.css" />
		
	</head>
	
	<body>
				
		<nav class="navbar navbar-toggleable-md navbar-right">

			<a class="navbar-brand" href="#"><img src="<?= Url::img('lanpoint_w_sm.png');?>" class="logo" /><img src="<?= Url::img('csgo.png');?>" style="margin-left: 10px" class="game" /></a>
			<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<?= Html::fa('bars'); ?>
			</button>
			
			<?php if (App::isAdmin()): ?><div class="admin-label">ADMINISTRATOR</div><?php endif; ?>
			
				
			<?php echo Nav::display(['items' => [
				[
					'label' => Html::fa('cogs').' Utils',
					'location' => Url::to('admin/utils'),
					'condition' => App::isAdmin()
				],
				[
					'label' => Html::fa('list').' Lista drużyn', 
					'location' => Url::to('team/list'), 
				],
				[
					'label' => Html::fa('users').' Lista użytkowników',
					'location' => Url::to('admin/usersList'),
					'condition' => App::isAdmin()
				],
				[
					'label' => Html::fa('user').' '.(App::loggedIn() ? App::$user->fullName : null), 
					'location' => Url::to(['user/view', 'id' => (App::loggedIn() ? App::$user->id : null)]), 
					'condition' => App::loggedIn()
				],
				[
					'label' => Html::fa('users').' '.(App::loggedIn() && App::$user->isMember ? App::$user->team->name : null), 
					'location' => Url::to(['team/view', 'id' => (App::loggedIn() && App::$user->isMember ? App::$user->team->id : null)]), 
					'condition' => App::loggedIn() && App::$user->isMember 
				],
				[
					'label' => Html::fa('sign-in').' Zaloguj się', 
					'location' => Url::to('site/login'), 
					'condition' => !App::loggedIn()
				],
				[
					'label' => Html::fa('sign-out').' Wyloguj się', 
					'location' => Url::to('site/logout'),
					'condition' => App::loggedIn()
				],
			]]); ?>
				
				
		</nav>

		<div class="container">
			
			<div class="content">
			
				<?php if (App::hasFlash()): ?>

				<div class="callouts">

					<?php 
					foreach (App::getAllFlashes() as $flash)
					{
						echo Callout::show($flash['type'], $flash['message']);
					}
					?>

				</div>

				<?php endif;?>
				
				<?= $content ?>
				
			</div>
			
		</div>
		
		<div id="push"></div>
		
		<footer>
			<div class="footer-content">
				&copy;2016 Radosław Stępiński
			</div>
		</footer>
		
	</body>
	
</html>