
<h1>Profil</h1>

<div class="row profile">
	
	<div class="col-lg-2 mb-3 mb-lg-0">
		
		<div class="avatar">
			
			<img src="<?= $user->avatarPath ?>" class="img-rounded"/>
			
		</div>
		
	</div>
	
	<div class="col-lg-10">
		
		<div class="name"><?= Html::fa('user', true).' '.$user->fullName ?></div>
		
		<div class="school"><?= Html::fa('graduation-cap', true).' '.$user->school ?></div>
		
		<div class="summoner <?= $user->steamId ? '' : 'inactive'?>"><?= Html::fa('gamepad', true).' '.($user->steamId
		? $user->steamName
		: 'Użytkownik nie powiązał swojego konta Steam.') ?>
		</div>
		
		<div class="team <?= $user->isMember ? '' : 'inactive'?>">
			
			<?= Html::fa('users', true).' '.($user->isMember ? Html::a(Url::to(['team/view', 'id' => $user->team->id]), $user->team->name) : 'Użytkownik nie dołączył do żadnej drużyny.') ?>
			
		</div>
		
		
		<?php if(App::loggedIn() && App::$user->id == $user->id): ?>
		
			<div class="edit">
				
				<?= Html::a(Url::to(['user/edit', 'id' => App::$user->id]), Html::fa('pencil').' Edytuj profil', ['class' => 'btn btn-warning btn-sm'], false) ?>
			
			</div>
		
		<?php endif; ?>
		
		<?php if (App::isAdmin()): ?>
		
		<div class="email">
			
			<?= Html::fa('envelope', true).' '.Html::a("mailto:$user->email", $user->email) ?>
			
		</div>
		
		<?= Html::a(Url::to(['admin/removeUser', 'id' => $user->id]), Html::fa('remove').' Usuń użytkownika', ['class' => 'btn btn-danger btn-sm'], false) ?>
		 
		<?php endif; ?>
		
	</div>
	
</div>