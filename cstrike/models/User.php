<?php

class User extends ActiveRecord implements IdentityInterface
{	
	const ROLE_USER = 0;
	const ROLE_ADMIN = 1;
	
	const AVATAR_SIZE = 400;
	const AVATAR_PATH = 'C:/xampp/htdocs/gorion/cstrike/site/img/avatars/';
	
	const SCENARIO_SIGNUP = 'signup';
	const SCENARIO_LOGIN = 'login';
	const SCENARIO_PAIR = 'pair';
	const SCENARIO_EDIT = 'edit';
	
	const POSITION_TOP = 0;
	const POSITION_MID = 2;
	const POSITION_JUNGLE = 1;
	const POSITION_SUPPORT = 3;
	const POSITION_ADC = 4;
	const POSITION_TBD = 5;
	
	const CLEAR = ['role', 'isMember', 'avatar', 'teamId', 'steamName', 'steamId'];
	
	function attributeLabels()
	{
		return [
			'username' => 'Login',
			'password' => 'Hasło',
			'firstName' => 'Imię',
			'lastName' => 'Nazwisko',
			'steamName' => 'Nazwa Steam',
			'steamId' => 'SteamID',
			'name' => 'Imię i nazwisko',
			'school' => 'Szkoła',
		];
	}
	
	function rules()
	{
		switch ($this->scenario)
		{
			case self::SCENARIO_SIGNUP: return [
				'username' => [
					'required', 
					['string', 'length' => ['max' => 30, 'min' => 3]], 
					['unique', 'field' => 'username', 'msg' => 'Ta nazwa jest zajęta']
				], 
				'email' => [
					'required', 
					'email', 
					['unique', 'field' => 'email', 'msg' => 'Ten email jest już w użyciu']
				], 
				'password' => [
					'required', 
					['string', 'length' => ['max' => 35, 'min' => 5]]
				],
				'firstName' => [
					'required', 
					['string', 'length' => ['max' => 35, 'min' => 5]]
				],
				'lastName' => [
					'required', 
					['string', 'length' => ['max' => 35, 'min' => 5]]
				],
				'school' => [['required']],
			];
			case self::SCENARIO_EDIT: return [
				'firstName' => ['required', ['string', 'length' => ['max' => 35, 'min' => 5]]],
				'lastName' => ['required', ['string', 'length' => ['max' => 35, 'min' => 5]]],
				'school' => [['required']],
			];
			case self::SCENARIO_LOGIN: return ['username' => ['required'], 'password' => ['required']];
			case self::SCENARIO_PAIR: return ['steamId' => ['required']];
		}
	}
	
	function updateName()
	{
		if ($this->steamId)
		{
			$data = App::valveAPI($this->steamId);
			
			$user = User::findOne(['id' => $this->id]);
			$user->steamName = $data->profile->playername;
			$user->update(false);
			
			App::updateUser();
		}
	}
	
	function getFullName()
	{
		return $this->firstName.' '.$this->lastName;
	}
	
	function getFullNameAndAlias()
	{
		return $this->firstName.' "'.$this->summonerName.'" '.$this->lastName;
	}
	
	function getTeam()
	{
		if (!$this->isMember) { return null; }
		return Team::findOne(['id' => $this->teamId]);
	}
	
	function getRequestedTeam()
	{
		if ($this->isMember) { return null; }
		return Team::findOne(['id' => $this->teamId]);
	}
	
	function getAvatarPath()
	{
		if ($this->hasAvatar)
		{
			return Url::img('avatars/'.$this->avatar, false);
		}
		else
		{
			return Url::img('avatars/no_avatar.png', false);
		}
	}
	
	function getHasAvatar()
	{
		return $this->avatar != 'no_avatar.png';
	}
	
	function login($password)
	{
		return password_verify($password, $this->password);
	}
}