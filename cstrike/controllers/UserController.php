<?php

class UserController extends BaseController
{
	function permissions()
	{
		return [
			'signup' => self::ACCESS_GUEST,
			'view' => self::ACCESS_GUEST,
		];
	}
	
	function actionSignup()
	{
		$model = new User(User::SCENARIO_SIGNUP);
		
		if ($model->load(App::$post))
		{
			if ($model->validate())
			{
				$model->password = password_hash($model->password, PASSWORD_DEFAULT);
				
				if ($model->save(false))
				{
					App::addFlash('success', 'Konto zostało utworzone. Możesz się teraz zalogować');
					return App::redirect(Url::to('site/login'));
				}
				
				App::addFlash('danger', 'Nie udało się utworzyć konta');
			}
			else
			{
				$model->flashErrors();
			}
		}
		
		return $this->render('user/signup');
	}
	
	function actionPair()
	{
		$model = new User(User::SCENARIO_PAIR);
		
		if (App::$user->steamId)
		{
			App::addFlash('warning', 'Już sparowałeś swoje konto. Jeśli chcesz je zmienić, najpierw usuń poprzednie powiązanie.');
		}
		else
		{
			if ($model->load(App::$post))
			{
				$data = App::valveAPI($model->steamId);
				
				if ($data instanceof stdClass || $data instanceof SimpleXMLElement)
				{				
					if (User::find()->where(['steam_id' => $data->profile->steamid64])->one())
					{
						App::addFlash('warning', 'To konto zostało już przypisane. Jeśli uważasz, że ktoś przypisał Twoje konto, napisz do nas maila.');
					}
					else
					{
						$user = App::$user;
						
						$user->steamName = $data->profile->playername;
						$user->steamId = $data->profile->steamid64;

						if ($user->update(false))
						{
							App::addFlash('success', 'Twoje konto zostało sparowane.');
							App::updateUser();
							$user->updateName();
							App::updateUser();
							return App::redirect(Url::to(['user/edit', 'id' => App::$user->id]));
						}
						else
						{
							App::addFlash('danger', 'Nie udało się powiązać twojego konta.');
						}
					}
				}
				else if ($data == App::ERR_API_ID_INCORRECT)
				{
					App::addFlash('danger', 'Podane przez ciebie steamID jest niepoprawne');
				}
				else if ($data == App::ERR_API_USER_NOT_FOUND)
				{
					App::addFlash('danger', 'Nie istnieje takie konto');
				}
				else if ($data == App::ERR_USER_VAC)
				{
					App::addFlash('danger', 'To konto ma blokadę Valve Anti-Cheat');
				}
				else if ($data == App::ERR_USER_BLACKLISTED)
				{
					App::addFlash('danger', 'To konto jest na czarnej liście cheaterów');
				}
				else
				{
					App::addFlash('danger', 'Wystąpił błąd po stronie serwera. Powiadom nas o tym.');
				}
			}
		}
		
		return $this->render('user/pair', ['user' => $model]);
	}
	
	function actionUnpair()
	{
		$user = App::$user;
		
		if ($user->team && $user->isMember)
		{
			App::addFlash('danger', 'Nie możesz rozłączyć konta, ponieważ jesteś członkiem drużyny. Opuść najpierw drużynę.');
		}
		else
		{
			$query = (new Query)->update(User::tableName(), ['steam_name' => '', 'steam_id' => 0])->where(['id' => $user->id]);
			if ($query->run())
			{
				App::addFlash('success', 'Rozłączono twoje konto Steam.');
				App::updateUser();
			}
			else
			{
				App::addFlash('danger', 'Nie udało się rozłączyć konta.');
			}
		}
		
		return App::redirect(Url::to(['user/edit', 'id' => $user->id]));
	}
	
	function actionView($id)
	{
		$model = User::findOne(['id' => $id]);
		
		if ($model === null)
		{
			App::addFlash('warning', 'Nie znaleziono strony');
			return App::goBack();
		}
		
		return $this->render('user/view', ['user' => $model]);
	}
	
	function actionEdit()
	{
		$model = App::$user;
		$model->scenario = User::SCENARIO_EDIT;
		
		if ($model->load(App::$post))
		{
			if ($model->update())
			{
				App::addFlash('success', "Zmiany zapisano.");
			}
			else
			{
				$model->flashErrors();
			}
			App::updateUser();
		}
		
		return $this->render('user/edit', ['user' => $model]);
	}
	
	function actionUpdateAvatar()
	{
		if (isset($_FILES['User:image']))
		{
			$file = App::fileUpload('User:image');
			
			if ($file)
			{
				$a = User::AVATAR_SIZE;
				
				$imagine = new \Imagine\Gd\Imagine;
				
				$image = $imagine->open($file);
				
				$size = $image->getSize();
				
				if ($size->getHeight() > $size->getWidth())
				{
					$newSize = new Imagine\Image\Box($a, $a * ($size->getHeight() / $size->getWidth()));
					$image->resize($newSize);
					
					$offset = ($newSize->getHeight() - $a) / 2;
					$image->crop(new Imagine\Image\Point(0, $offset), new Imagine\Image\Box($a, $a));
				}
				else if ($size->getHeight() < $size->getWidth())
				{
					$newSize = new Imagine\Image\Box($a, $a * ($size->getWidth() / $size->getHeight()));
					$image->resize($newSize);
					
					$offset = ($newSize->getWidth() - $a) / 2;
					$image->crop(new Imagine\Image\Point($offset, 0), new Imagine\Image\Box($a, $a));
				}
				else
				{
					$image->resize(new Imagine\Image\Box($a, $a));
				}
				
				$path = md5(App::$user->username.rand(0,1000)).'.jpg';
				if ($image->save(User::AVATAR_PATH.$path))
				{
					if ((new Query)->update(User::tableName(), ['avatar' => $path])->where(['id' => App::$user->id])->run())
					{
						unlink($file);
						App::addFlash('success', 'Awatar został zmieniony.');
						App::updateUser();
						return App::redirect(Url::to(['user/edit', 'id' => App::$user->id]));
					}
					else
					{
						App::addFlash('danger', 'Nie udało się połączyć z bazą danych.');
					}
				}
				else
				{
					App::addFlash('danger', 'Nie udało się zmienić awatara.');
				}
			}
		}
		
		return $this->render('user/uploadAvatar', ['user' => App::$user]);
	}
	
	function actionDeleteAvatar()
	{
		if (App::$user->avatar != 'no_avatar.png')
		{
			$path = App::$user->avatar;
			if ((new Query)->update(User::tableName(), ['avatar' => 'no_avatar.png'])->where(['id' => App::$user->id])->run())
			{
				unlink($path);
				App::addFlash('success', 'Usunięto awatar.');
				App::updateUser();
			}
			else
			{
				App::addFlash('danger', 'Nie udało się usunąć awatara.');
			}
		}
		return App::redirect(Url::to(['user/edit', 'id' => App::$user->id]));
	}
	
	function actionLeaveTeam()
	{
		$user = App::$user;
		$team = $user->team ?: $user->requestedTeam;
		
		if ($team)
		{
			if (!$team->isLocked)
			{
				if ($user->id == $team->ownerId)
				{
					$members = $team->members;

					if (count($members) == 1)
					{
						$user->teamId = null;
						$user->isMember = false;
						if($team->delete() && $user->update(false))
						{
							App::addFlash('success', 'Opuściłeś drużynę. Ponieważ byłeś jedynym jej członkiem, została usunięta.');
						}
					}
					else
					{
						$user->teamId = null;
						$user->isMember = false;
						if ($user->update(false))
						{
							$team = Team::findOne(['id' => $team->id]);
							$count = count($team->members);
							$newOwner = $team->members[rand(0, $count-1)];
							$team->ownerId = $newOwner->id;

							if ($team->update(false))
							{
								App::addFlash('success', "Opuściłeś drużynę. Ponieważ byłeś jej właścicielem, przekazano uprawnienia użytkownikowi $newOwner->summonerName");
							}
							else
							{
								App::addFlash('danger', 'Wystąpił błąd podczas połączenia z bazą danych');
							}
						}
						else
						{
							App::addFlash('danger', 'Wystąpił błąd podczas połączenia z bazą danych');
						}
					}
					App::updateUser();
				}
				else
				{
					$member = $user->isMember;
					$user->isMember = false;
					$user->teamId = null;
					if ($user->update(false))
					{
						App::addFlash('success', $member ? 'Opuściłeś drużynę' : 'Cofnięto prośbę');
						App::updateUser();
					}
					else
					{
						App::addFlash('danger', $member ? 'Nie udało się opuścić drużyny' : 'Nie udało się cofnąć prośby');
					}
				}
			}
			else
			{
				App::addFlash('danger', 'Ta drużyna jest zablokowana. Nie możesz jej opuścić. '.($user->id == $team->ownerId ? 'Odblokuj ją.' : 'Poproś właściciela, by odblokował drużynę.'));
			}
		}
		
		return App::goBack();
	}
}