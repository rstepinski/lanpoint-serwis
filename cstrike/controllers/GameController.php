<?php

class GameController extends BaseController
{
	function permissions()
	{
		return [
			'create' => self::ACCESS_ADMIN,
			'view' => self::ACCESS_GUEST,
		];
	}
	
	function actionCreate($teams)
	{
		$game = new Game;
		
		$game->teams = $teams[0]->id.','.$teams[1]->id;
		$game->tournamentCode = App::generateTournamentCode();
		$game->callbackToken = App::generateToken();
	}
	
	function actionCallback($token)
	{
		$data = new APICallback;
		
		$game = Game::findOne(['id' => $game->id]);
		
		if ($game !== null)
		{
			if ($data->load(App::$post))
			{
				if ($game->callbackToken != $token)					
				{
					$game->error = Game::ERR_TOKEN_MISMATCH;
				}
				
				$teams = [$data->winningTeam, $data->losingTeam];
				
				foreach ($teams as $teamData)
				{
					$team = null;
					$first = true;
					foreach ($teamData as $member)
					{
						$user = User::findOne(['summoner_id' => $member->summonerId]);
						if ($user === null) { $game->error |= GAME::ERR_SUMMONER_MISMATCH; break; }
						if ($first)
						{
							$team = $user->team;
							if ($user->team->id != $game->teams[0]->id || $user->team->id != $game->teams[1]->id)
							{
								$game->error |= GAME::ERR_TEAM_MISMATCH; break;
							}
						}
						else
						{
							if ($team->id != $user->team->id)
							{
								$game->error |= GAME::ERR_SUMMONER_MISMATCH; break;
							}
						}
						$first = false;
					}
				}
				
				$game->gameId = $data->gameId;
				
				if ($game->error != 0)
				{
					$game->save();
				}
				else
				{
				}
			}
		}
	}
	
	function actionView($id)
	{
		
	}
}